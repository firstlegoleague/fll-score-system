function timer_start() {

    var audio = new Audio('../sounds/start.mp3');
    audio.play();

    timer = setInterval(function() {
        //
        var elementMin = document.getElementById("timer_min");
        var elementSec = document.getElementById("timer_sec");

        // Parse to interget
        var min = parseInt(elementMin.innerText);
        var sec = parseInt(elementSec.innerText);

        // Do the minute calculation
        if(min == 0 && sec == 0){
            clearInterval(timer)
        }
        else if(sec == 0){
            sec = 60
            min--;
        }

        // Remove a second
        sec--;


        // Prevent negative time.
        if(sec == -1){
            sec = 0
            // Play sound
            var audio = new Audio('../sounds/end.mp3');
            audio.play();
        }

        if(min == 0 && sec == 30){
            var audio = new Audio('../sounds/end-game.mp3');
            audio.play();
        }

        // Render the second
        if(sec < 10){
            elementSec.innerText = '0' + sec;
        } else {
            elementSec.innerText = sec;
        }

        // Render the minute
        elementMin.innerText = min;

    }, 1000);
}

function timer_stop() {
    var audio = new Audio('../sounds/stop.mp3');
    audio.play();
    clearInterval(timer)
}

function timer_reset() {
    clearInterval(timer)
    var elementMin = document.getElementById("timer_min");
    var elementSec = document.getElementById("timer_sec")

    elementMin.innerText = "2";
    elementSec.innerText = "30";

}

function timer_test_end() {
    var audio = new Audio('../sounds/end.mp3');
    audio.play();
}

function timer_test_stop() {
    var audio = new Audio('../sounds/stop.mp3');
    audio.play();
}

function timer_test_start() {
    var audio = new Audio('../sounds/start.mp3');
    audio.play();
}

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}
