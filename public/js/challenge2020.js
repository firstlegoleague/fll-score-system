
function js_M00() {
    var score = 0;
    var element = document.getElementById("M00");
    var M00 = document.getElementsByName("M00_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M00[1].checked) {
        score = 25;
    }

    //Check if it valid
    for (var i = 0, length = M00.length; i < length; i++) {
        if (M00[i].checked) {
            check = check + 1;
            element.classList.add("alert-success");
            break;
        }
    }

    document.getElementById("M00_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M01() {
    var score = 0;
    var element = document.getElementById("M01");
    var M01_1 = document.getElementsByName("M01_1");
    var M01_2 = document.getElementsByName("M01_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    for (var i = 0, length = M01_1.length; i < length; i++) {
        if (M01_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M01_2.length; i < length; i++) {
        if (M01_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(M01_1[1].checked){
        if(M01_2[1].checked || M01_2[0].checked){
            score = 20;
        }
        else {
            score = 0;
        }
    } else {
        score = 0;
    }

    if(check == 2)
        element.classList.add("alert-success");

    document.getElementById("M01_pt").innerText = score + " pt";

    if (check != 2) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M02() {
    var score = 0;
    var element = document.getElementById("M02");
    var M02_1 = document.getElementsByName("M02_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M02_1[1].checked){
        score = 10;
    } else if(M02_1[2].checked){
        score = 15;
    } else if(M02_1[3].checked){
        score = 20;
    }
    document.getElementById("M02_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M02_1.length; i < length; i++) {
        if (M02_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
    }

    if(check != 1) {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M03() {

    var score = 0;
    var element = document.getElementById("M03");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var error = document.getElementById("M03-error");
    error.innerText = "";
    error.classList.remove("alert");

    var M03_1 = document.getElementsByName("M03_1");
    var M03_2 = document.getElementsByName("M03_2");
    var M03_3 = document.getElementsByName("M03_3");
    var check = 0;

    // Validity check
    for (var i = 0, length = M03_1.length; i < length; i++) {
        if (M03_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M03_2.length; i < length; i++) {
        if (M03_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M03_3.length; i < length; i++) {
        if (M03_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    // Conflicts test
    if(M03_1[0].checked){
        if (M03_2[1].checked || M03_3[1].checked) {
            check = 0;
            element.classList.add("alert-danger");
            error.innerText = "Error: Te veel glijbaanfiguren";
            error.classList.add("alert");
        }
    }

    if(M03_1[1].checked){
        if (M03_2[1].checked && M03_3[1].checked) {
            check = 0;
            element.classList.add("alert-danger");
            error.innerText = "Error: Te veel glijbaanfiguren";
            error.classList.add("alert");
        }
    }

    if(check == 3) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M03_1[1].checked){
            score = score + 5
        } else if(M03_1[2].checked){
            score = score + 20
        }

        if(M03_2[1].checked){
            score = score + 10;
        }

        if(M03_3[1].checked){
            score = score + 20;
        }


    }
    document.getElementById("M03_pt").innerText = score + " pt";

    if(check != 3) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M04() {

    var score = 0;
    var element = document.getElementById("M04");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var M04_1 = document.getElementsByName("M04_1");
    var M04_2 = document.getElementsByName("M04_2");
    var M04_3 = document.getElementsByName("M04_3");
    var check = 0;

    // Validity check
    for (var i = 0, length = M04_1.length; i < length; i++) {
        if (M04_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M04_2.length; i < length; i++) {
        if (M04_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M04_3.length; i < length; i++) {
        if (M04_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 3) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M04_1[1].checked) {
            score = score + 10;
            for (var i = 0, length = M04_3.length; i < length; i++) {
                if (M04_3[i].checked) {
                    score = score + i * 10;
                    break;
                }
            }
        }

        if(M04_2[1].checked){
            score = score + 15;
        }
    }

    document.getElementById("M04_pt").innerText = score + " pt";

    if(check != 3) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M05() {

    var score = 0;
    var element = document.getElementById("M05");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var M05_1 = document.getElementsByName("M05_1");
    var M05_2 = document.getElementsByName("M05_2");

    var check = 0;

    // Validity check
    for (var i = 0, length = M05_1.length; i < length; i++) {
        if (M05_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M05_2.length; i < length; i++) {
        if (M05_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M05_1[1].checked) {
            score = score + 15;
        }
        if(M05_2[1].checked){
            score = score + 15;
        } else if(M05_2[2].checked){
            score = score + 25;
        }
    }

    document.getElementById("M05_pt").innerText = score + " pt";

    if(check != 2) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M06M07_check(){
    var errorM06 = document.getElementById("M06-error");
    errorM06.innerText = "";
    errorM06.classList.remove("alert");

    var errorM07 = document.getElementById("M07-error");
    errorM07.innerText = "";
    errorM07.classList.remove("alert");

    var M06_2 = document.getElementsByName("M06_2");
    var M07_1 = document.getElementsByName("M07_1");

    var elementM06 = document.getElementById("M06");
    var elementM07 = document.getElementById("M07");
    elementM06.classList.remove("alert-danger");
    elementM07.classList.remove("alert-danger");


    var check = 0

    if(M06_2[1].checked){
        check = check + 1
    }

    if(M07_1[1].checked){
        check = check + 1
    }

    if(check == 2){
        elementM06.classList.add("alert-danger");
        errorM06.innerText = "De robot is op 2 locaties tijdens het einde van het spel";
        errorM06.classList.add("alert");

        elementM07.classList.add("alert-danger");
        errorM07.innerText = "De robot is op 2 locaties tijdens het einde van het spel";
        errorM07.classList.add("alert");
    }





}

function js_M06() {

    var score = 0;
    var element = document.getElementById("M06");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var M06_1 = document.getElementsByName("M06_1");
    var M06_2 = document.getElementsByName("M06_2");
    var check = 0;

    // Validity check
    for (var i = 0, length = M06_1.length; i < length; i++) {
        if (M06_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M06_2.length; i < length; i++) {
        if (M06_2[i].checked) {
            check = check + 1;
            break;
        }
    }


    if(check == 2) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Check if its valid with M06 / M07
        js_M06M07_check();

        // Score check | Only if field is valid
        if(M06_1[1].checked){
            score = score + 15
        }

        if(M06_2[1].checked){
            score = score + 30;
        }
    }
    document.getElementById("M06_pt").innerText = score + " pt";

    if(check != 2) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M07() {

    var score = 0;
    var element = document.getElementById("M07");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var M07_1 = document.getElementsByName("M07_1");
    var check = 0;

    // Validity check
    for (var i = 0, length = M07_1.length; i < length; i++) {
        if (M07_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Check if its valid with M06 / M07
        js_M06M07_check();

        // Score check | Only if field is valid
        if(M07_1[1].checked){
            score = score + 20
        }

    }
    document.getElementById("M07_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M08() {

    var score = 0;
    var element = document.getElementById("M08");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var M08_1 = document.getElementsByName("M08_1");
    var M08_2 = document.getElementsByName("M08_2");
    var M08_3 = document.getElementsByName("M08_3");
    var M08_4 = document.getElementById("M08_blockAmount");
    var check = 0;

    // Validity check
    for (var i = 0, length = M08_1.length; i < length; i++) {
        if (M08_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M08_2.length; i < length; i++) {
        if (M08_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M08_3.length; i < length; i++) {
        if (M08_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 3) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M08_1[1].checked){
            score = score + 25
        }

        if(M08_2[1].checked){
            score = score + 10
        }

        //score = score + M08_4.value * 5;
        score = score + M08_4.value * 5;

        if(M08_3[1].checked){
            score = 0;
        }

    }
    document.getElementById("M08_pt").innerText = score + " pt";

    if(check != 3) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M09() {

    var score = 0;
    var element = document.getElementById("M09");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var M09_1 = document.getElementsByName("M09_1");
    var M09_2 = document.getElementsByName("M09_2");
    var M09_3 = document.getElementsByName("M09_3");
    var check = 0;

    // Validity check
    for (var i = 0, length = M09_1.length; i < length; i++) {
        if (M09_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M09_2.length; i < length; i++) {
        if (M09_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M09_3.length; i < length; i++) {
        if (M09_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 3) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M09_1[1].checked){
            score = score + 10;
        }
        if(M09_1[2].checked){
            score = score + 15;
        }
        if(M09_1[3].checked){
            score = score + 25;
        }

        if(M09_2[1].checked ){
            if(M09_1[1].checked || M09_1[3].checked) {
                score = score + 5;
            }
        }

        if(M09_2[2].checked ){
            if(M09_1[2].checked || M09_1[3].checked) {
                score = score + 5;
            }
        }

        if(M09_2[3].checked ){
            if(M09_1[3].checked) {
                score = score + 10;
            }
        }

        if(M09_3[1].checked){
            if(M09_1[2].checked || M09_1[3].checked) {
                score = score - 15;


                if (M09_2[2].checked || M09_2[3].checked) {
                    score = score - 5;
                }
            }
        }

    }
    document.getElementById("M09_pt").innerText = score + " pt";

    if(check != 3) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M10() {
    var score = 0;
    var element = document.getElementById("M10");
    var M10_1 = document.getElementsByName("M10_1");
    var check = 0;

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    //Check if it valid
    for (var i = 0, length = M10_1.length; i < length; i++) {
        if (M10_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
        if(M10_1[1].checked) {
            //document.getElementById("M00_pt").innerText = "25 pt"
            score = 15;
        }
    }

    document.getElementById("M10_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M11() {
    var score = 0;
    var element = document.getElementById("M11");
    var M11_1 = document.getElementsByName("M11_1");
    var check = 0;

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    //Check if it valid
    for (var i = 0, length = M11_1.length; i < length; i++) {
        if (M11_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
        for (var i = 0, length = M11_1.length; i < length; i++) {
            if (M11_1[i].checked) {
                score = score + i*5;
                break;
            }
        }
    }

    document.getElementById("M11_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M12() {

    var score = 0;
    var element = document.getElementById("M12");
    element.classList.remove("alert-danger");

    var error = document.getElementById("M12-error");
    error.innerText = "";
    error.classList.remove("alert");

    var M12_1 = document.getElementsByName("M12_1");
    var M12_2 = document.getElementsByName("M12_2");
    var check = 0;

    // Validity check
    for (var i = 0, length = M12_1.length; i < length; i++) {
        if (M12_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M12_2.length; i < length; i++) {
        if (M12_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    // Error check
    if(M12_2[1].checked && M12_1[0].checked){
        check = 0;
        element.classList.remove("alert-success");
        element.classList.add("alert-danger");
        error.classList.add("alert")
        error.innerText = "Error: Conflict positie wiel roeimachine";

    }

    if(check == 2) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M12_1[1].checked){
            score = score + 15
        }
        if(M12_2[1].checked){
            score = score + 15
        }

    }
    document.getElementById("M12_pt").innerText = score + " pt";

    if(check != 2) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M13() {

    var score = 0;
    var element = document.getElementById("M13");
    element.classList.remove("alert-danger");


    var M13_1 = document.getElementsByName("M13_1");
    var check = 0;

    // Validity check
    for (var i = 0, length = M13_1.length; i < length; i++) {
        if (M13_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M13_1[1].checked){
            score = score + 10
        }
        if(M13_1[2].checked){
            score = score + 15
        }
        if(M13_1[3].checked){
            score = score + 20
        }


    }
    document.getElementById("M13_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M14() {

    var score = 0;
    var element = document.getElementById("M14");
    element.classList.remove("alert-danger");

    var errorM14 = document.getElementById("M14-error");
    errorM14.innerText = "";
    errorM14.classList.remove("alert");

    var M14_1 = document.getElementById("M14_healthpoints");
    var M14_2 = document.getElementsByName("M14_2");
    var check = 0;

    // Validity check
    for (var i = 0, length = M14_2.length; i < length; i++) {
        if (M14_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        var amount_1 = M14_1.value;
        var amount_2 = 0

        // Score check | Only if field is valid
        score = score + M14_1.value * 5;

        for (var i = 0, length = M14_2.length; i < length; i++) {
            if (M14_2[i].checked) {
                amount_2 = i
                score = score + i * 10;
                break;
            }
        }

        var amount = Number(amount_1) + Number(amount_2);

        console.log(amount)
        if(amount > 8){

            element.classList.add("alert-danger");
            errorM14.innerText = "Error: Te veel gezondheidsunits";
            errorM14.classList.add("alert");
            score = 0;

        }

    }
    document.getElementById("M14_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;

}

function js_M15() {

    var score = 0;
    var element = document.getElementById("M15");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");


    var M15_1 = document.getElementsByName("M15_1");
    var check = 0;

    // Validity check
    for (var i = 0, length = M15_1.length; i < length; i++) {
        if (M15_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M15_1[1].checked){
            score = 5
        } else if(M15_1[2].checked){
            score = 10
        }else if(M15_1[3].checked){
            score = 20
        }else if(M15_1[4].checked){
            score = 30
        }else if(M15_1[5].checked){
            score = 45
        }else if(M15_1[6].checked){
            score = 60
        }
    }
    document.getElementById("M15_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }
    return score;

}

function M14_1_add() {
    var M14_1 = document.getElementById("M14_healthpoints");
    M14_1.value++;
    js_M14()
    return;
}

function M14_1_deduct() {
    var M14_1 = document.getElementById("M14_healthpoints");
    M14_1.value = M14_1.value - 1;
    if(M14_1.value == -1){
        M14_1.value = 0;
    }
    js_M14()
    return;
}

function M08_4_add() {
    var M14_1 = document.getElementById("M08_blockAmount");
    M14_1.value++;
    if(M14_1.value == 19){
        M14_1.value = 18
    }
    js_M08()
    return;
}

function M08_4_deduct() {
    var M14_1 = document.getElementById("M08_blockAmount");
    M14_1.value = M14_1.value - 1;
    if(M14_1.value == -1){
        M14_1.value = 0;
    }
    js_M08()
    return;
}

function M08_4_add_start(){
    M08_4_counter_add = setInterval(function() {
        var M14_1 = document.getElementById("M08_blockAmount");
        M14_1.value++;
        if(M14_1.value == 19){
            M14_1.value = 18;
        }
    }, 240);
    js_M08()
}

function M08_4_add_stop(){
    clearInterval(M08_4_counter_add)
}

function M08_4_deduct_start(){
    M08_4_counter_deduct = setInterval(function() {
        var M14_1 = document.getElementById("M08_blockAmount");
        M14_1.value--;
        if(M14_1.value == -1){
            M14_1.value = 0;
        }
    }, 500);
    js_M08()
}

function M08_4_deduct_stop(){
    clearInterval(M08_4_counter_deduct)
}

function M14_1_add_start(){
    M14_1_counter_add = setInterval(function() {
        var M14_1 = document.getElementById("M14_healthpoints");
        M14_1.value++;
    }, 240);
    js_M14()
}

function M14_1_add_stop(){
    clearInterval(M14_1_counter_add)
}

function M14_1_deduct_start(){
    M14_1_counter_deduct = setInterval(function() {
        var M14_1 = document.getElementById("M14_healthpoints");
        M14_1.value--;
        if(M14_1.value == -1){
            M14_1.value = 0;
        }
    }, 500);
    js_M14()
}

function M14_1_deduct_stop(){
    clearInterval(M14_1_counter_deduct)
}

function calcScore(){

    var score = 0;
    var fail = 0;

    js_M00();
    if (js_M00() != -1){
        score = score + Number(js_M00())
    }else {
        fail = 1
    }

    js_M01();
    if (js_M01() != -1){
        score = score + Number(js_M01())
    }else {
        fail = 1
    }

    js_M02();
    if (js_M02() != -1){
        score = score + Number(js_M02())
    }else {
        fail = 1
    }

    js_M03();
    if (js_M03() != -1){
        score = score + Number(js_M03())
    }else {
        fail = 1
    }

    js_M04();
    if (js_M04() != -1){
        score = score + Number(js_M04())
    }else {
        fail = 1
    }

    js_M05();
    if (js_M05() != -1){
        score = score + Number(js_M05())
    }else {
        fail = 1
    }

    js_M06();
    if (js_M06() != -1){
        score = score + Number(js_M06())
    }else {
        fail = 1
    }

    js_M07();
    if (js_M07() != -1){
        score = score + Number(js_M07())
    }else {
        fail = 1
    }

    js_M08();
    if (js_M08() != -1){
        score = score + Number(js_M08())
    }else {
        fail = 1
    }

    js_M09();
    if (js_M09() != -1){
        score = score + Number(js_M09())
    }else {
        fail = 1
    }

    js_M10();
    if (js_M10() != -1){
        score = score + Number(js_M10())
    }else {
        fail = 1
    }

    js_M11();
    if (js_M11() != -1){
        score = score + Number(js_M11())
    }else {
        fail = 1
    }

    js_M12();
    if (js_M12() != -1){
        score = score + Number(js_M12())
    }else {
        fail = 1
    }

    js_M13();
    if (js_M13() != -1){
        score = score + Number(js_M13())
    }else {
        fail = 1
    }

    js_M14();
    if (js_M14() != -1){
        score = score + Number(js_M14())
    }else {
        fail = 1
    }

    js_M15();
    if (js_M15() != -1){
        score = score + Number(js_M15())
    } else {
        fail = 1
    }

    console.log("Score: " + score);
    console.log("Fail: " + fail);

    if(fail == 1){
        return -1
    } else {
        document.getElementById("total_pt").innerText = "Total: " + score + " pt";
        var btn = document.getElementById("submitButton");

        btn.disabled = false;

        return score
    }

}
