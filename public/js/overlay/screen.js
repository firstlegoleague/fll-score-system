/**
 * Script file for display overlay page
 * @author: Marcin van de Ven
 */
// Hide content to wait for load
$("#overlay_content").hide();

$(function() {
    // Initialize variables
    var configJson = null;
    var settings = null;
    /**
     * Loads the settings from a 'local' json-file
     * @TODO: change to API-URL when ready.
     */
    $.getJSON('js/overlay/testdata.json', function (response) {

        var scoreboard = document.getElementById("content-screen");
        var title = document.getElementById('title-bar');


        configJson = response;
        console.log("Settings file '" + configJson.name + "' loaded");
        settings = configJson.settings;
        rankings = configJson.rankings;

        title.innerHTML = "<h1>" + settings.title + "</h1>";

        console.log(settings.background);



        for ( i in rankings){

            console.log(rankings[i].ranking);
            var table = document.getElementById("myTable");
            var row = table.insertRow(i);
            var rank = row.insertCell(0);
            var number = row.insertCell(1);
            var team = row.insertCell(2);
            var score = row.insertCell(3);

            rank.innerHTML = rankings[i].ranking;
            number.innerHTML = rankings[i].teamnumber;
            team.innerHTML = rankings[i].teamname;
            score.innerHTML = rankings[i].score;

            if ( i % 2 == 0){
                row.style = row.style + "backgroud-color: " + settings.background_even + ";";
            }

        }


        console.log(scoreboard.innerHTML)

        // Change settings such a background and colors
        $("body").css({
            'background': settings.background,
            'color': settings.text_color
        });

        // Switch loader to page
        $("#overlay_load").hide();
        $("#overlay_content").show();
    })
});


var my_time;
var count = 0;
var lastScrollTop = 0;
setTimeout('pageScroll()', 2500);

function pageScroll() {
    // If condition to set repeat

    var objDiv = document.getElementById("content-screen");

    if ((objDiv.scrollTop == lastScrollTop)) {
        setTimeout(function() {
            lastScrollTop = -1;
            objDiv.scrollTop = 1;
            my_time = setTimeout('pageScroll()', 2500);
        }, 2500);
    } else {
        lastScrollTop = objDiv.scrollTop;
        objDiv.scrollTop = objDiv.scrollTop + 1;
        my_time = setTimeout('pageScroll()', 150);
    }
    //set scrolling time start

    //set scrolling time end

}
