function js_togglePublic(id, state){

    var url = "/rounds/";
    url = url + id;
    var element = document.getElementById("toggle_" + id);

    if(state == 0){
        url = url + "/public";
        element.classList.add('alert-success');
        element.classList.remove('alert-danger');
        element.innerHTML = "<i class=\"far fa-eye\"></i>";
    } else {
        url = url + "/private";
        element.classList.add('alert-danger');
        element.classList.remove('alert-success');
        element.innerHTML = "<i class=\"far fa-eye-slash\"></i>";
    }

    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send();

    return;
}

var my_time;
var count = 0;
var lastScrollTop = 0;
setTimeout('pageScroll()', 2500);

function pageScroll() {
    var objDiv = document.getElementById("content-screen");

    if ((objDiv.scrollTop == lastScrollTop)) {
        setTimeout(function() {
            lastScrollTop = -1;
            objDiv.scrollTop = 1;
            my_time = setTimeout('pageScroll()', 2500);
        }, 2500);
    } else {
        lastScrollTop = objDiv.scrollTop;
        objDiv.scrollTop = objDiv.scrollTop + 1;
        my_time = setTimeout('pageScroll()', 150);
    }
}
