# ![Alt text](public/img/logos/fll.png "Title") FLL Scoring

## About FLL Scoring
FLL scoring is an open source tool to be used in tournaments or practices to keep score. The need for this tool began 
during the covid period. The BeNeLux finale couldn't use the existing tools for the way this final was set up. So a 
custom tool was build. After lots of positive feedback from the volunteers who used it, a desision was made to contiune 
the development of this tool.

## Contributing
This being an open source tool, every one is encouraged to help the development of this tool. This can be adding a
feature that you like to see, add your langauge to it, or something entirely different. This can be done by forking
the entire tool, create the function, languages file. When the function is stable, create a merge request.

## Security Vulnerabilities
If you discover a security vulnerability within FLL scoring, please send an e-mail to us via [thijs@fllscoring.nl](mailto:thijs@fllscoring.nl). All security vulnerabilities will be promptly addressed.

## License
FLL scoring is open-sourced software licensed under the [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/).

## Want to use it
If you want to use this tool, but have no knowledge on how to set it up? We provide hosted solutions for finals. Please contact me via [thijs@fllscoring.nl](mailto:thijs@fllscoring.nl) and we will respond as quick as possible.

## Volunteering
All work we do is volunteering, so it sometimes might take some time for us to respond to issues, feature requests or other things. Please be aware of that.

## Installing
On how to install it, or develop, please see INSTALL.md.

## Contact
We can be reached via email [thijs@fllscoring.nl](mailto:thijs@fllscoring.nl) or on Libera.chat in the channel [#fllscoring](https://web.libera.chat/#fllscoring).
