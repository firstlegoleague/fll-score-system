#!/usr/bin/python3

import secrets
import string
from shutil import copyfile
import os
alphabet = string.ascii_letters + string.digits

#os.remove("mysql-script-1.sql")
mysqlscript = open("mysql-script-1.sql", "w")
copyfile("./.env.example", "./.env")
envfile = open(".env", "a")

username = "fll"
database = "fll"
password = ''.join(secrets.choice(alphabet) for i in range(24))

print("Write this down somewhere! (NOT SOMEWHERE, IN THE KEEPASS / BITWARDEN!)")
print("The password is: {}".format(password))

mysqlscript.write("CREATE USER '{}'@'localhost' IDENTIFIED BY '{}';\n".format(username, password))
mysqlscript.write("GRANT ALL PRIVILEGES ON {}.* TO '{}'@'localhost';\n".format(database, username))
mysqlscript.write("CREATE DATABASE {};\n".format(database))
mysqlscript.write("FLUSH PRIVILEGES;\n")

envfile.write("DB_DATABASE={}\nDB_USERNAME={}\nDB_PASSWORD={}".format(database, username, password))

mysqlscript.close()
envfile.close()

