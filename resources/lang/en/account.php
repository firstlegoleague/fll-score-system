<?php

return array (
  'changeConfirm' => 'Change',
  'changepassword' => 'Change your password',
  'confirmPassword' => 'Confirm your new password',
  'currentPassword' => 'Current password',
  'forcePassword' => 'Force password',
  'name' => 'Name',
  'newPassword' => 'New password',
  'title' => 'Your Account',
);
