<?php

return array (
  'account' => 'Account',
  'gamelist' => 'Game list',
  'login' => 'Login',
  'logout' => 'Logout',
  'newForm' => 'New scoreform',
  'overview-gametable' => 'Game table',
  'rounds' => 'Rounds',
  'scoreboard-public' => 'Scoreboard',
  'settings' => 'Settings',
  'tables' => 'Tables',
  'teams' => 'Teams',
  'timer' => 'Timer',
  'users' => 'Users',
);
