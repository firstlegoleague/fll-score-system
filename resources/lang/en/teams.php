<?php

return array (
  'ID' => 'ID',
  'add' => 'Add a team',
  'affiliate' => 'Affiliate',
  'edit' => 'Edit',
  'list' => 'Teams list',
  'name' => 'Team name',
  'number' => 'Team number',
  'savedsuccess' => 'The team has been changed successful',
);
