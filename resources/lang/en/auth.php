<?php

return array (
  'changePassword' => 'Change password',
  'failed' => 'These credentials do not match our records.',
  'forgotPassword' => 'Forgot Your Password?',
  'login' => 'Login',
  'password' => 'Password',
  'rememberme' => 'Onthoud mij',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
);
