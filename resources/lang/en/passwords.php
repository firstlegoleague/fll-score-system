<?php

return array (
  'cantMatchOld' => 'New Password cannot be same as your current password. Please choose a different password.',
  'changeSuccess' => 'Password changed successfully !',
  'dontMatch' => 'Your current password does not matches with the password you provided. Please try again.',
  'reset' => 'Your password has been reset!',
  'sent' => 'We have e-mailed your password reset link!',
  'throttled' => 'Please wait before retrying.',
  'token' => 'This password reset token is invalid.',
  'user' => 'We can\'t find a user with that e-mail address.',
);
