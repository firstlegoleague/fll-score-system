<?php

return array (
  'finalname' => 'Final Name',
  'registerEnabled' => 'Enable registering',
  'scorePublic' => 'Scoreboards public accessible',
  'season' => 'Season',
  'submit' => 'Save',
  'title' => 'Settings',
);
