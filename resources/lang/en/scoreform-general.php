<?php

return array (
  'amountBricks' => 'Amount bricks',
  'checkForm' => 'Check score sheet and calculate score',
  'gameInfo' => 'Game Info',
  'remarks' => 'Remarks',
  'round' => 'Round',
  'saved_successfull' => 'The score sheet has been saved successfully',
  'scoreform' => 'Score sheet',
  'submit' => 'Submit',
  'table' => 'Table',
  'team' => 'Team',
);
