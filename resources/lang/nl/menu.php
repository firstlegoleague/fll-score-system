<?php

return array (
  'account' => 'Account',
  'gamelist' => 'Wedstrijduitslagen',
  'login' => 'Inloggen',
  'logout' => 'Uitloggen',
  'newForm' => 'Nieuw formulier',
  'overview-gametable' => 'Wedstrijdoverzicht',
  'rounds' => 'Rondes',
  'scoreboard-public' => 'Scorebord',
  'settings' => 'Instellingen',
  'tables' => 'Tafels',
  'teams' => 'Teams',
  'timer' => 'Timer',
  'users' => 'Gebruikers',
);
