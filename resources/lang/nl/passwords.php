<?php

return array (
  'cantMatchOld' => 'Je nieuwe wachtwoord mag niet hetzelfde zijn als je oude wachtwoord. Kies iets anders.',
  'changeSuccess' => 'Je wachtwoord is succesvol gewijzigd!',
  'dontMatch' => 'Je wachtwoord klopt niet met wat je hebt ingevuld.',
  'reset' => 'Je wachtwoord is gereset!',
  'sent' => 'We hebben een e-mail gestuurd met een link om je wachtwoord te resetten!',
  'throttled' => 'Wacht even met het opnieuw proberen',
  'token' => 'Dit wachtwoord reset token is niet meer geldig',
  'user' => 'Er is geen gebruiker met dit emailadres gevonden',
);
