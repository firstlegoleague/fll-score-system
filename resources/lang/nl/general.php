<?php

return array (
  'add' => 'Toevoegen',
  'close' => 'Sluiten',
  'delete' => 'Verwijderen',
  'dontforgettosend' => 'Vergeet het formulier niet te versturen!',
  'download' => 'Download',
  'edit' => 'Aanpassen',
  'email' => 'Email',
  'footer-volunteer' => 'Alle regiopartners en vrijwilligers, bedankt!',
  'id' => 'ID',
  'name' => 'Naam',
  'published' => 'Openbaar',
  'rank' => 'Plaats',
  'save' => 'Opslaan',
  'score' => 'Score',
  'total' => 'Totaal',
'results' => 'Scores',
'downloadScores' => 'Download Scores (OJS)',
'downloadRemarks' => 'Download Opmerkingen',
);
