<?php

return array (
  'ID' => 'ID',
  'add' => 'Voeg team toe',
  'affiliate' => 'Organisatie',
  'edit' => 'Aanpassen',
  'list' => 'Teamlijst',
  'name' => 'Teamnaam',
  'number' => 'Teamnummer',
  'savedsuccess' => 'Het team is succesvol aangepast',
);
