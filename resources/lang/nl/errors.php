<?php

return array (
  '401_subtitle' => 'Error 401 : Unautorized',
  '401_text' => 'Jij hebt hier niets te zoeken!',
  '401_title' => 'Emmet verbaast zich dat je hier probeert te komen.',
  '403_subtitle' => 'Je mag hier niet komen',
  '403_text' => '403 Verboden',
  '403_title' => 'Je hebt niet de juiste permissies om hier te komen',
  '404_subtitle' => 'Ooops, Emmit kan deze pagina niet vinden, maar geen zorgen! Everything is still awesome',
  '404_text' => 'Daarnaast zijn er nu andere legofiguurtjes bezig met de fouten in de website te zoeken en op te lossen!',
  '404_title' => 'Error 404: Pagina niet gevonden',
);
