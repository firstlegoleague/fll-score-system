<?php

return array (
  'changePassword' => 'Wijzig wachtwoord',
  'failed' => 'Deze gegevens komen niet voor in onze database',
  'forgotPassword' => 'Wachtwoord vergeten?',
  'login' => 'Login',
  'password' => 'Wachtwoord',
  'rememberme' => 'Onthoud mij',
  'throttle' => 'Rustig aan, je het het te vaak geprobeerd. Je mag een nieuwe poging wagen over :seconds seconde.',
);
