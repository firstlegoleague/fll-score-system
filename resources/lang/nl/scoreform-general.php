<?php

return array (
  'amountBricks' => 'Aantal blokjes',
  'checkForm' => 'Controleer scoreformulier en bereken score',
  'gameInfo' => 'Ronde Info',
  'remarks' => 'Opmerkingen',
  'round' => 'Ronde',
  'saved_successfull' => 'Het scoreformulier is succesvol opgeslagen',
  'scoreform' => 'Scoreformulier',
  'submit' => 'Versturen',
  'table' => 'Tafel',
  'team' => 'Team',
);
