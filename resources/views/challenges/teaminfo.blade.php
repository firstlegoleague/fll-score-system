<div class="card" xmlns="http://www.w3.org/1999/html">
    <div class="card-header">{{__('scoreform-general.gameInfo')}}</div>
    <div class="card-body">

        {{-- The team selection box --}}
        <div class="form-group row ">
            <label class="col-md-2 col-form-label text-md-right" for="team">{{__('scoreform-general.team')}}</label>
            <div class="col-md-8">
                <input required class="form-control" list="teams" id="team" name="team"/>
                <datalist id="teams">
                    @foreach($teams as $team)
                        <option value="{{$team->teamNumber}} {{$team->teamname}}"></option>
                    @endforeach
                </datalist>
            </div>
        </div>

        {{-- The round selection --}}
        <div class="form-group row ">
            <label class="col-md-2 col-form-label text-md-right" for="team">{{__('scoreform-general.round')}}</label>
            <div class="col-md-8">
                <select required class="form-control" list="rounds" id="round" name="round"/>
                    @foreach($rounds as $round)
                        <option value="{{$round->round}}">{{$round->round}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
