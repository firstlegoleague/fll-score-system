@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2>{{__('challenge2021.title')}} {{__('scoreform-general.scoreform')}}</h2>

                @if (session()->has('successSaved'))
                    <div class="alert alert-success" role="alert">
                        {{__('scoreform-general.saved_successfull')}}
                    </div>
                @endif

                <form autocomplete="off" method="POST"
                      action="{{ route('game.edit.post.2021', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}">
                    @csrf

                    <input type="hidden" id="game_id" name="game_id" value="{{$game->id}}">

                    <div class="card">
                        <div class="card-header">{{__('scoreform-general.gameInfo')}}</div>
                        <div class="card-body">

                            {{-- The team selection box --}}
                            <div class="form-group row ">
                                <label class="col-md-2 col-form-label text-md-right"
                                       for="team">{{__('scoreform-general.team')}}</label>
                                <div class="col-md-8">
                                    <input type="text" readonly class="form-control" list="teams" id="team" name="team"
                                           value="{{\App\Teams::all()->where('id',$game->teamID)->first()->teamname}}">
                                </div>
                            </div>

                            {{-- The round selection --}}
                            <div class="form-group row ">
                                <label class="col-md-2 col-form-label text-md-right" for="team">{{__('scoreform-general.round')}}</label>
                                <div class="col-md-8">
                                    <select required class="form-control" list="rounds" id="round" name="round"/>
                                    @foreach($rounds as $round)
                                        <option
                                            @if($game->roundID == $round->id)
                                                selected
                                            @endif
                                            value="{{$round->round}}">{{$round->round}}</option>
                                        @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-- Mission 00 | Inspectiones --}}
                    <div class="card scoreform-spacer">
                        <div id="M00" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M00-name')}}</div>
                            <div id="M00_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body">{{__('challenge2021.M00-desc')}}</div>

                        <div class="card-body radio-toolbar">
                            {{__('challenge2021.M00-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M00_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M00();" type="radio" id="m00_no" name="M00_1" value="0">
                                <label onclick="js_M00();" for="m00_no">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M00_1 == 1)
                                       checked
                                       @endif required onclick="js_M00();" type="radio"
                                       id="m00_yes" name="M00_1" value="1">
                                <label onclick="js_M00();" for="m00_yes">{{__('challenge2021.yes')}}</label>

                            </div>
                        </div>
                    </div>

                    {{-- Mission 01 | Innovation Project --}}
                    <div class="card scoreform-spacer">

                        <div id="M01" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M01-name')}}</div>
                            <div id="M01_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">
                            {{__('challenge2021.M01-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M01_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M01();" type="radio" id="m01_1_no" name="M01_1" value="0">
                                <label onclick="js_M01();" for="m01_1_no">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M01_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M01();" type="radio" id="m01_1_yes" name="M01_1" value="1">
                                <label onclick="js_M01();" for="m01_1_yes">{{__('challenge2021.yes')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 02 | Unused Capacity --}}
                    <div class="card scoreform-spacer">
                        <div id="M02" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M02-name')}}</div>
                            <div id="M02_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">
                            {{__('challenge2021.M02-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M02_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M02();" type="radio" id="m02_1_no" name="M02_1" value="0">
                                <label onclick="js_M02();" for="m02_1_no">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M02_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M02();" type="radio" id="m02_1_yes" name="M02_1" value="1">
                                <label onclick="js_M02();" for="m02_1_yes">{{__('challenge2021.yes')}}</label>
                            </div>

                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M02-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M02_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M02();" type="radio" id="m02_2_0" name="M02_2" value="0">
                                <label onclick="js_M02();" for="m02_2_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M02_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M02();" type="radio" id="m02_2_1-5" name="M02_2" value="1">
                                <label onclick="js_M02();" for="m02_2_1-5">1-5</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M02_2 == 2)
                                       checked
                                       @endif
                                       required onclick="js_M02();" type="radio" id="m02_2_6" name="M02_2" value="2">
                                <label onclick="js_M02();" for="m02_2_6">6</label>
                            </div>

                        </div>


                    </div>

                    {{-- Mission 03 | Unload Cargo Plane --}}
                    <div class="card scoreform-spacer">
                        <div id="M03" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M03-name')}}</div>
                            <div id="M03_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger" id="M03-error"></div>

                            {{__('challenge2021.M03-scoring1')}}

                            <div class="fll-beside fll-right">
                                <input @if($game->M03_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M03();" type="radio" id="m03_1_no" name="M03_1" value="0">
                                <label onclick="js_M03();" for="m03_1_no">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M03_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M03();" type="radio" id="m03_1_yes" name="M03_1" value="1">
                                <label onclick="js_M03();" for="m03_1_yes">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M03-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M03_2 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M03();" type="radio" id="m03_2_no" name="M03_2" value="0">
                                <label onclick="js_M03();" for="m03_2_no">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M03_2 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M03();" type="radio" id="m03_2_yes" name="M03_2" value="1">
                                <label onclick="js_M03();" for="m03_2_yes">{{__('challenge2021.yes')}}</label>
                            </div>
                        </div>
                    </div>

                    {{-- Mission 04 | Transportation Journey --}}
                    <div class="card scoreform-spacer">
                        <div id="M04" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M04-name')}}</div>
                            <div id="M04_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            {{__('challenge2021.M04-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M04_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M04();" type="radio" id="m04_1_no" name="M04_1" value="0">
                                <label onclick="js_M04();" for="m04_1_no">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M04_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M04();" type="radio" id="m04_1_yes" name="M04_1" value="1">
                                <label onclick="js_M04();" for="m04_1_yes">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M04-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M04_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M04();" type="radio" id="m04_2_nee" name="M04_2" value="0">
                                <label onclick="js_M04();" for="m04_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M04_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M04();" type="radio" id="m04_2_ja" name="M04_2" value="1">
                                <label onclick="js_M04();" for="m04_2_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                        </div>
                    </div>

                    {{-- Mission 05 | Switch Engine --}}
                    <div class="card scoreform-spacer">
                        <div id="M05" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M05-name')}}</div>
                            <div id="M05_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            {{__('challenge2021.M05-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M05_1 == 0)
                                       checked
                                       @endif required onclick="js_M05();" type="radio" id="m05_1_nee" name="M05_1"
                                       value="0">
                                <label onclick="js_M05();" for="m05_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M05_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M05();" type="radio" id="m05_1_ja" name="M05_1" value="1">
                                <label onclick="js_M05();" for="m05_1_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                        </div>
                    </div>

                    {{-- Mission 06 | Pull-Up Bar --}}
                    <div class="card scoreform-spacer">
                        <div id="M06" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M06-name')}}</div>
                            <div id="M06_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger" id="M06-error"></div>

                            {{__('challenge2021.M06-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M06_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M06();" type="radio" id="m06_1_nee" name="M06_1" value="0">
                                <label onclick="js_M06();" for="m06_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M06_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M06();" type="radio" id="m06_1_ja" name="M06_1" value="1">
                                <label onclick="js_M06();" for="m06_1_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M06-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M06_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M06();" type="radio" id="m06_2_nee" name="M06_2" value="0">
                                <label onclick="js_M06();" for="m06_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M06_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M06();" type="radio" id="m06_2_ja" name="M06_2" value="1">
                                <label onclick="js_M06();" for="m06_2_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M06-scoring3')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M06_3 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M06();" type="radio" id="m06_3_nee" name="M06_3" value="0">
                                <label onclick="js_M06();" for="m06_3_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M06_3 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M06();" type="radio" id="m06_3_ja" name="M06_3" value="1">
                                <label onclick="js_M06();" for="m06_3_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 07 | Unload Cargo Ship --}}
                    <div class="card scoreform-spacer">
                        <div id="M07" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M07-name')}}</div>
                            <div id="M07_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger" id="M07-error"></div>

                            {{__('challenge2021.M07-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M07_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M07();" type="radio" id="m07_1_nee" name="M07_1" value="0">
                                <label onclick="js_M07();" for="m07_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M07_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M07();" type="radio" id="m07_1_ja" name="M07_1" value="1">
                                <label onclick="js_M07();" for="m07_1_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M07-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M07_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M07();" type="radio" id="m07_2_nee" name="M07_2" value="0">
                                <label onclick="js_M07();" for="m07_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M07_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M07();" type="radio" id="m07_2_ja" name="M07_2" value="1">
                                <label onclick="js_M07();" for="m07_2_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 08 | Air Drop --}}
                    <div class="card scoreform-spacer">
                        <div id="M08" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M08-name')}}</div>
                            <div id="M08_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="alert-danger hideError alert fll-error" id="M08-error">{{__('challenge2021.M08-error')}}</div>

                        <div class="card-body radio-toolbar">

                            {{__('challenge2021.M08-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M08_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M08();" type="radio" id="m08_1_nee" name="M08_1" value="0">
                                <label onclick="js_M08();" for="m08_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M08_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M08();" type="radio" id="m08_1_ja" name="M08_1" value="1">
                                <label onclick="js_M08();" for="m08_1_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M08-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M08_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M08();" type="radio" id="m08_2_nee" name="M08_2" value="0">
                                <label onclick="js_M08();" for="m08_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M08_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M08();" type="radio" id="m08_2_ja" name="M08_2" value="1">
                                <label onclick="js_M08();" for="m08_2_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M08-scoring3')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M08_3 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M08();" type="radio" id="m08_3_nee" name="M08_3" value="0">
                                <label onclick="js_M08();" for="m08_3_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M08_3 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M08();" type="radio" id="m08_3_ja" name="M08_3" value="1">
                                <label onclick="js_M08();" for="m08_3_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                        </div>
                    </div>

                    {{-- Mission 09 | Train Tracks --}}
                    <div class="card scoreform-spacer">
                        <div id="M09" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M09-name')}}</div>
                            <div id="M09_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger hideError" id="M09-error">{{__('challenge2021.M09-error')}}</div>

                            {{__('challenge2021.M09-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M09_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M09();" type="radio" id="m09_1_nee" name="M09_1" value="0">
                                <label onclick="js_M09();" for="m09_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M09_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M09();" type="radio" id="m09_1_ja" name="M09_1" value="1">
                                <label onclick="js_M09();" for="m09_1_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M09-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M09_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M09();" type="radio" id="m09_2_nee" name="M09_2" value="0">
                                <label onclick="js_M09();" for="m09_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M09_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M09();" type="radio" id="m09_2_ja" name="M09_2" value="1">
                                <label onclick="js_M09();" for="m09_2_ja">{{__('challenge2021.yes')}}</label>
                            </div>


                        </div>
                    </div>

                    {{-- Mission 10 | Sorting Center --}}
                    <div class="card scoreform-spacer">
                        <div id="M10" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M10-name')}}</div>
                            <div id="M10_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            {{__('challenge2021.M10-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M10_1 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M10();" type="radio" id="m10_1_nee" name="M10_1" value="0">
                                <label onclick="js_M10();" for="m10_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M10_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M10();" type="radio" id="m10_1_ja" name="M10_1" value="1">
                                <label onclick="js_M10();" for="m10_1_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 11 | Home Delivery --}}
                    <div class="card scoreform-spacer">
                        <div id="M11" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M11-name')}}</div>
                            <div id="M11_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            {{__('challenge2021.M11-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M11_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M11();" type="radio" id="m11_1_nee" name="M11_1" value="0">
                                <label onclick="js_M11();" for="m11_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M11_1 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M11();" type="radio" id="m11_1_partly" name="M11_1"
                                    value="1">
                                <label onclick="js_M11();" for="m11_1_partly">{{__('challenge2021.partly')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M11_1 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M11();" type="radio" id="m11_1_completely" name="M11_1"
                                    value="2">
                                <label onclick="js_M11();"
                                       for="m11_1_completely">{{__('challenge2021.completely')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 12 | Large Devlivery --}}
                    <div class="card scoreform-spacer">
                        <div id="M12" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M12-name')}}</div>
                            <div id="M12_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger" id="M12-error"></div>

                            {{__('challenge2021.M12-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M12_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M12();" type="radio" id="m12_1_nee" name="M12_1" value="0">
                                <label onclick="js_M12();" for="m12_1_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M12_1 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M12();" type="radio" id="m12_1_mat" name="M12_1" value="1">
                                <label onclick="js_M12();" for="m12_1_mat">{{__('challenge2021.M12-mat')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M12_1 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M12();" type="radio" id="m12_1_nothing" name="M12_1"
                                    value="2">
                                <label onclick="js_M12();"
                                       for="m12_1_nothing">{{__('challenge2021.M12-nothing')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M12-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M12_2 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M12();" type="radio" id="m12_2_nee" name="M12_2" value="0">
                                <label onclick="js_M12();" for="m12_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M12_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M12();" type="radio" id="m12_2_partly" name="M12_2"
                                       value="1">
                                <label onclick="js_M12();" for="m12_2_partly">{{__('challenge2021.partly')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M12_2 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M12();" type="radio" id="m12_2_completely" name="M12_2"
                                    value="2">
                                <label onclick="js_M12();"
                                       for="m12_2_completely">{{__('challenge2021.completely')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 13 | Platooning Trucks --}}
                    <div class="card scoreform-spacer">
                        <div id="M13" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M13-name')}}</div>
                            <div id="M13_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            {{__('challenge2021.M13-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M13_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M13();" type="radio" id="m13_nee" name="M13_1" value="0">
                                <label onclick="js_M13();" for="m13_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M13_1 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M13();" type="radio" id="m13_ja" name="M13_1" value="1">
                                <label onclick="js_M13();" for="m13_ja">{{__('challenge2021.yes')}}</label>
                            </div>
                            <div class="fll-spacer"></div>

                            {{__('challenge2021.M13-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input @if($game->M13_2 == 0)
                                       checked
                                       @endif
                                       required onclick="js_M13();" type="radio" id="m13_2_nee" name="M13_2" value="0">
                                <label onclick="js_M13();" for="m13_2_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input @if($game->M13_2 == 1)
                                       checked
                                       @endif
                                       required onclick="js_M13();" type="radio" id="m13_2_ja" name="M13_2" value="1">
                                <label onclick="js_M13();" for="m13_2_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 14 | Bridge --}}
                    <div class="card scoreform-spacer">
                        <div id="M14" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M14-name')}}</div>
                            <div id="M14_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger" id="M14-error"></div>

                            {{__('challenge2021.M14-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M14_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M14();" type="radio" id="M14_1_0" name="M14_1" value="0">
                                <label onclick="js_M14();" for="M14_1_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M14_1 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M14();" type="radio" id="M14_1_1" name="M14_1" value="1">
                                <label onclick="js_M14();" for="M14_1_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M14_1 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M14();" type="radio" id="M14_1_2" name="M14_1" value="2">
                                <label onclick="js_M14();" for="M14_1_2">2</label>

                            </div>

                        </div>
                    </div>

                    {{-- Mission 15 | Load Cargo --}}
                    <div class="card scoreform-spacer">
                        <div id="M15" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M15-name')}}</div>
                            <div id="M15_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="alert-danger" id="M15-error"></div>

                            {{__('challenge2021.M15-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M15_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_1_0" name="M15_1" value="0">
                                <label onclick="js_M15();" for="M15_1_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M15_1 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_1_1" name="M15_1" value="1">
                                <label onclick="js_M15();" for="M15_1_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M15_1 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_1_2" name="M15_1" value="2">
                                <label onclick="js_M15();" for="M15_1_2">{{__('challenge2021.M15-more')}}</label>
                            </div>
                            <div class="fll-spacer"></div>
                            {{__('challenge2021.M15-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M15_2
 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_2_0" name="M15_2" value="0">
                                <label onclick="js_M15();" for="M15_2_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M15_2 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_2_1" name="M15_2" value="1">
                                <label onclick="js_M15();" for="M15_2_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M15_2 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_2_2" name="M15_2" value="2">
                                <label onclick="js_M15();" for="M15_2_2">{{__('challenge2021.M15-more')}}</label>
                            </div>
                            <div class="fll-spacer"></div>
                            {{__('challenge2021.M15-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M15_3 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_3_0" name="M15_3" value="0">
                                <label onclick="js_M15();" for="M15_3_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M15_3 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_3_1" name="M15_3" value="1">
                                <label onclick="js_M15();" for="M15_3_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M15_3 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M15();" type="radio" id="M15_3_2" name="M15_3" value="2">
                                <label onclick="js_M15();" for="M15_3_2">{{__('challenge2021.M15-more')}}</label>
                            </div>

                        </div>
                    </div>

                    {{-- Mission 16 | Cargo Connect --}}
                    <div class="card scoreform-spacer">
                        <div id="M16" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.M16-name')}}</div>
                            <div id="M16_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="alert-danger hideError alert fll-error"
                             id="M16-error">{{__('challenge2021.M16-error')}}</div>

                        <div class="card-body radio-toolbar">
                            {{__('challenge2021.M16-scoring1')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M16_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_0" name="M16_1" value="0">
                                <label onclick="js_M16();" for="M16_1_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_1" name="M16_1" value="1">
                                <label onclick="js_M16();" for="M16_1_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_2" name="M16_1" value="2">
                                <label onclick="js_M16();" for="M16_1_2">2</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 3)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_3" name="M16_1" value="3">
                                <label onclick="js_M16();" for="M16_1_3">3</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 4)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_4" name="M16_1" value="4">
                                <label onclick="js_M16();" for="M16_1_4">4</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 5)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_5" name="M16_1" value="5">
                                <label onclick="js_M16();" for="M16_1_5">5</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 6)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_6" name="M16_1" value="6">
                                <label onclick="js_M16();" for="M16_1_6">6</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 7)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_7" name="M16_1" value="7">
                                <label onclick="js_M16();" for="M16_1_7">7</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_1 == 8)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_1_8" name="M16_1" value="8">
                                <label onclick="js_M16();" for="M16_1_8">8</label>
                            </div>

                            <div class="fll-spacer"></div>
                            {{__('challenge2021.M16-scoring2')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M16_2 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_0" name="M16_2" value="0">
                                <label onclick="js_M16();" for="M16_2_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_1" name="M16_2" value="1">
                                <label onclick="js_M16();" for="M16_2_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_2" name="M16_2" value="2">
                                <label onclick="js_M16();" for="M16_2_2">2</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 3)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_3" name="M16_2" value="3">
                                <label onclick="js_M16();" for="M16_2_3">3</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 4)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_4" name="M16_2" value="4">
                                <label onclick="js_M16();" for="M16_2_4">4</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 5)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_5" name="M16_2" value="5">
                                <label onclick="js_M16();" for="M16_2_5">5</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 6)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_6" name="M16_2" value="6">
                                <label onclick="js_M16();" for="M16_2_6">6</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 7)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_7" name="M16_2" value="7">
                                <label onclick="js_M16();" for="M16_2_7">7</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_2 == 8)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_2_8" name="M16_2" value="8">
                                <label onclick="js_M16();" for="M16_2_8">8</label>
                            </div>
                            <div class="fll-spacer"></div>
                            {{__('challenge2021.M16-scoring3')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M16_3 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_3_nee" name="M16_3" value="0">
                                <label onclick="js_M16();" for="M16_3_nee">{{__('challenge2021.no')}}</label>

                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_3 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_3_ja" name="M16_3" value="1">
                                <label onclick="js_M16();" for="M16_3_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                            <div class="fll-spacer"></div>
                            {{__('challenge2021.M16-scoring4')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M16_4 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="m16_4_nee" name="M16_4" value="0">
                                <label onclick="js_M16();" for="m16_4_nee">{{__('challenge2021.no')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_4 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="m16_4_ja" name="M16_4" value="1">
                                <label onclick="js_M16();" for="m16_4_ja">{{__('challenge2021.yes')}}</label>
                            </div>

                            <div class="fll-spacer"></div>
                            {{__('challenge2021.M16-scoring5')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M16_5 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_0" name="M16_5" value="0">
                                <label onclick="js_M16();" for="M16_5_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_5 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_1" name="M16_5" value="1">
                                <label onclick="js_M16();" for="M16_5_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_5 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_2" name="M16_5" value="2">
                                <label onclick="js_M16();" for="M16_5_2">2</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_5 == 3)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_3" name="M16_5" value="3">
                                <label onclick="js_M16();" for="M16_5_3">3</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_5 == 4)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_4" name="M16_5" value="4">
                                <label onclick="js_M16();" for="M16_5_4">4</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_5 == 5)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_5" name="M16_5" value="5">
                                <label onclick="js_M16();" for="M16_5_5">5</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M16_5 == 6)
                                    checked
                                    @endif
                                    required onclick="js_M16();" type="radio" id="M16_5_6" name="M16_5" value="6">
                                <label onclick="js_M16();" for="M16_5_6">6</label>

                            </div>
                        </div>
                    </div>

                    {{-- Mission 17 | Precision --}}
                    <div class="card scoreform-spacer">
                        <div id="M17" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.precision-name')}}</div>
                            <div id="M17_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body">{{__('challenge2021.precision-desc')}}</div>

                        <div class="card-body radio-toolbar">
                            {{__('challenge2021.precision-scoring')}}
                            <div class="fll-beside fll-right">
                                <input
                                    @if($game->M17_1 == 0)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_0" name="M17_1" value="0">
                                <label onclick="js_M17();" for="M17_0">0</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M17_1 == 1)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_1" name="M17_1" value="1">
                                <label onclick="js_M17();" for="M17_1">1</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M17_1 == 2)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_2" name="M17_1" value="2">
                                <label onclick="js_M17();" for="M17_2">2</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M17_1 == 3)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_3" name="M17_1" value="3">
                                <label onclick="js_M17();" for="M17_3">3</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M17_1 == 4)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_4" name="M17_1" value="4">
                                <label onclick="js_M17();" for="M17_4">4</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M17_1 == 5)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_5" name="M17_1" value="5">
                                <label onclick="js_M17();" for="M17_5">5</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->M17_1 == 6)
                                    checked
                                    @endif
                                    required onclick="js_M17();" type="radio" id="M17_6" name="M17_1" value="6">
                                <label onclick="js_M17();" for="M17_6">6</label>
                            </div>

                        </div>
                    </div>

                    {{-- Gracious Professialism  --}}
                    <div class="card scoreform-spacer">
                        <div id="GP" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.gp-text')}}</div>
                        </div>

                        <div class="card-body radio-toolbar">

                            <div class="fll-beside fll-right">
                                <input @if($game->GP_1 == 2)
                                       checked
                                       @endif
                                       required onclick="js_GP();" type="radio" id="GP_1_0" name="GP_1" value="2">
                                <label onclick="js_GP();" for="GP_1_0">{{__('challenge2021.developing')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->GP_1 == 3)
                                    checked
                                    @endif required onclick="js_GP();" type="radio" id="GP_1_1" name="GP_1"
                                    value="3">
                                <label onclick="js_GP();" for="GP_1_1">{{__('challenge2021.accomplished')}}</label>
                                <div class="fll-beside" style="width: 20px"></div>
                                <input
                                    @if($game->GP_1 == 4)
                                    checked
                                    @endif required onclick="js_GP();" type="radio" id="GP_1_2" name="GP_1"
                                    value="4">
                                <label onclick="js_GP();" for="GP_1_2">{{__('challenge2021.exceeds')}}</label>

                            </div>

                        </div>
                    </div>


                    @can('scoreform.save')
                        {{-- Returns --}}
                        <div class="card scoreform-spacer">
                            <div id="notition" class="card-header">
                                <div class="fll-beside">{{__('challenge2021.return')}}</div>
                            </div>

                            <div class="card-body r">
                                {{__('challenge2021.returns')}}
                            </div>
                        </div>


                        {{-- Remarks --}}
                        <div class="card scoreform-spacer">
                            <div class="card-header">
                                <div class="fll-beside">{{__('scoreform-general.remarks')}}</div>
                            </div>

                            <div class="card-body r">
                    <textarea rows="3" cols="100" id="remarks" name="remarks"
                              placeholder="{{__('scoreform-general.remarks')}}?">{{$game->remarks}}
                    </textarea>
                            </div>
                        </div>
                    @endcan

                    {{-- Submit --}}
                    <div class="card scoreform-spacer">
                        <div id="score" class="card-header">
                            <div class="fll-beside">{{__('scoreform-general.submit')}}</div>
                            <div id="total_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">
                            <input type="button" onclick="calcScore()"
                                   value="{{__('scoreform-general.checkForm')}}">
                        </div>

                        <input type="hidden" name="totalScore" id="total_pt_form">

                        @can('scoreform.save')
                            <div class="card-body radio-toolbar">
                                <input type="submit" value="{{__('scoreform-general.submit')}}" id="submitButton"
                                       disabled>
                            </div>
                        @endcan
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/challenge2021.js') }}" defer></script>
@endsection
