@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>{{__('challenge2020.title')}} {{__('scoreform-general.scoreform')}}</h2>

            @can('scoreform.save')
            {{-- Some information about the game itself --}}
                {{-- The team selection box --}}
                <div class="form-group row ">
                    <label class="col-md-2 col-form-label text-md-right"
                           for="team">{{__('scoreform-general.team')}}</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" list="teams" id="team" name="team"
                               value="{{\App\Teams::all()->where('id',$game->teamID)->first()->teamname}}">
                    </div>
                </div>

                {{-- The round selection --}}
                <div class="form-group row ">
                    <label class="col-md-2 col-form-label text-md-right"
                           for="team">{{__('scoreform-general.round')}}</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" list="rounds" id="round"
                               name="round" value="{{\App\Rounds::all()->where('id',$game->roundID)->first()->round}}">
                    </div>
                </div>
            @endcan
            <form autocomplete="off" method="POST" action="{{ route('games.edit.2020', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}">
                @csrf

            {{-- Mission 00 | Inspectiones --}}
            <div class="card scoreform-spacer">
                <div id="M00" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M00-name')}}</div>
                    <div id="M00_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M00-desc')}}</div>

                <div class="card-body radio-toolbar">
                    {{__('challenge2020.M00-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M00();" type="radio" id="m00_no" name="M00_1" value="0">
                        <label onclick="js_M00();" for="m00_no">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M00();" type="radio" id="m00_yes" name="M00_1" value="1">
                        <label onclick="js_M00();" for="m00_yes">{{__('challenge2020.yes')}}</label>

                    </div>
                </div>
            </div>

            {{-- Mission 01 | Innovation Project --}}
            <div class="card scoreform-spacer">

                <div id="M01" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M01-name')}}</div>
                    <div id="M01_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M01-desc')}}</div>

                <div class="card-body radio-toolbar">
                    {{__('challenge2020.M01-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M01();" type="radio" id="m01_1_no" name="M01_1" value="0">
                        <label onclick="js_M01();" for="m01_1_no">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M01();" type="radio" id="m01_1_yes" name="M01_1" value="1">
                        <label onclick="js_M01();" for="m01_1_yes">{{__('challenge2020.yes')}}</label>
                    </div>

                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M01-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M01();" type="radio" id="m01_2_logo" name="M01_2" value="0">
                        <label onclick="js_M01();" for="m01_2_logo">{{__('challenge2020.M01-logo')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M01();" type="radio" id="m01_2_gray" name="M01_2" value="1">
                        <label onclick="js_M01();" for="m01_2_gray">{{__('challenge2020.M01-gray')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M01();" type="radio" id="m01_2_none" name="M01_2" value="2">
                        <label onclick="js_M01();" for="m01_2_none">{{__('challenge2020.None')}}</label>
                    </div>
                </div>
            </div>

            {{-- Mission 02 | Step Couinter --}}
            <div class="card scoreform-spacer">
                <div id="M02" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M02-name')}}</div>
                    <div id="M02_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M02-desc')}}</div>

                <div class="card-body radio-toolbar">
                    {{__('challenge2020.M01-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M02();" type="radio" id="m02_1_none" name="M02_1" value="0">
                        <label onclick="js_M02();" for="m02_1_none">{{__('challenge2020.None')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M02();" type="radio" id="m02_1_magenta" name="M02_1" value="1">
                        <label onclick="js_M02();" for="m02_1_magenta">{{__('challenge2020.Magenta')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M02();" type="radio" id="m02_1_yellow" name="M02_1" value="2">
                        <label onclick="js_M02();" for="m02_1_yellow">{{__('challenge2020.Yellow')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M02();" type="radio" id="m02_1_blue" name="M02_1" value="3">
                        <label onclick="js_M02();" for="m02_1_blue">{{__('challenge2020.Blue')}}</label>
                    </div>
                </div>
            </div>

            {{-- Mission 03 | Slide --}}
            <div class="card scoreform-spacer">
                <div id="M03" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M03-name')}}</div>
                    <div id="M03_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M03-desc')}}</div>

                <div class="card-body radio-toolbar">

                    <div class="alert-danger" id="M03-error"></div>

                    {{__('challenge2020.M03-scoring1')}}

                    <div class="fll-beside fll-right">
                        <input required onclick="js_M03();" type="radio" id="m03_1_0" name="M03_1" value="0">
                        <label onclick="js_M03();" for="m03_1_0">0</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M03();" type="radio" id="m03_1_1" name="M03_1" value="1">
                        <label onclick="js_M03();" for="m03_1_1">1</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M03();" type="radio" id="m03_1_2" name="M03_1" value="2">
                        <label onclick="js_M03();" for="m03_1_2">2</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M03-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M03();" type="radio" id="m03_2_no" name="M03_2" value="0">
                        <label onclick="js_M03();" for="m03_2_no">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M03();" type="radio" id="m03_2_yes" name="M03_2" value="1">
                        <label onclick="js_M03();" for="m03_2_yes">{{__('challenge2020.yes')}}</label>
                    </div>

                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M03-scoring3')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M03();" type="radio" id="m03_3_no" name="M03_3" value="0">
                        <label onclick="js_M03();" for="m03_3_no">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M03();" type="radio" id="m03_3_yes" name="M03_3" value="1">
                        <label onclick="js_M03();" for="m03_3_yes">{{__('challenge2020.yes')}}</label>
                    </div>
                </div>
            </div>

            {{-- Mission 04 | Bench --}}
            <div class="card scoreform-spacer">
                <div id="M04" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M04-name')}}</div>
                    <div id="M04_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M04-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M04-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M04();" type="radio" id="m04_1_no" name="M04_1" value="0">
                        <label onclick="js_M04();" for="m04_1_no">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M04();" type="radio" id="m04_1_yes" name="M04_1" value="1">
                        <label onclick="js_M04();" for="m04_1_yes">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M04-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M04();" type="radio" id="m04_2_nee" name="M04_2" value="0">
                        <label onclick="js_M04();" for="m04_2_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M04();" type="radio" id="m04_2_ja" name="M04_2" value="1">
                        <label onclick="js_M04();" for="m04_2_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M04-scoring3')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M04();" type="radio" id="m04_3_0" name="M04_3" value="0">
                        <label onclick="js_M04();" for="m04_3_0">0</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M04();" type="radio" id="m04_3_1" name="M04_3" value="1">
                        <label onclick="js_M04();" for="m04_3_1">1</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M04();" type="radio" id="m04_3_2" name="M04_3" value="2">
                        <label onclick="js_M04();" for="m04_3_2">2</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M04();" type="radio" id="m04_3_3" name="M04_3" value="3">
                        <label onclick="js_M04();" for="m04_3_3">3</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M04();" type="radio" id="m04_3_4" name="M04_3" value="4">
                        <label onclick="js_M04();" for="m04_3_4">4</label>
                    </div>
                </div>
            </div>

            {{-- Mission 05 | Basketball --}}
            <div class="card scoreform-spacer">
                <div id="M05" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M05-name')}}</div>
                    <div id="M05_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M05-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M05-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M05();" type="radio" id="m05_1_nee" name="M05_1" value="0">
                        <label onclick="js_M05();" for="m05_1_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M05();" type="radio" id="m05_1_ja" name="M05_1" value="1">
                        <label onclick="js_M05();" for="m05_1_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M05-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M05();" type="radio" id="m05_2_none" name="M05_2" value="0">
                        <label onclick="js_M05();" for="m05_2_none">{{__('challenge2020.None')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M05();" type="radio" id="m05_2_middle" name="M05_2" value="1">
                        <label onclick="js_M05();" for="m05_2_middle">{{__('challenge2020.M05-middle')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M05();" type="radio" id="m05_2_top" name="M05_2" value="2">
                        <label onclick="js_M05();" for="m05_2_top">{{__('challenge2020.M05-top')}}</label>
                    </div>
                </div>
            </div>

            {{-- Mission 06 | Pull-Up Bar --}}
            <div class="card scoreform-spacer">
                <div id="M06" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M06-name')}}</div>
                    <div id="M06_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M06-desc')}}</div>

                <div class="card-body radio-toolbar">

                    <div class="alert-danger" id="M06-error"></div>

                    {{__('challenge2020.M06-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M06();" type="radio" id="m06_1_nee" name="M06_1" value="0">
                        <label onclick="js_M06();" for="m06_1_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M06();" type="radio" id="m06_1_ja" name="M06_1" value="1">
                        <label onclick="js_M06();" for="m06_1_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M06-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M06();" type="radio" id="m06_2_nee" name="M06_2" value="0">
                        <label onclick="js_M06();" for="m06_2_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M06();" type="radio" id="m06_2_ja" name="M06_2" value="1">
                        <label onclick="js_M06();" for="m06_2_ja">{{__('challenge2020.yes')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 07 | Robot Dance --}}
            <div class="card scoreform-spacer">
                <div id="M07" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M07-name')}}</div>
                    <div id="M07_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M07-desc')}}</div>

                <div class="card-body radio-toolbar">

                    <div class="alert-danger" id="M07-error"></div>

                    {{__('challenge2020.M07-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M07();" type="radio" id="m07_1_nee" name="M07_1" value="0">
                        <label onclick="js_M07();" for="m07_1_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M07();" type="radio" id="m07_1_ja" name="M07_1" value="1">
                        <label onclick="js_M07();" for="m07_1_ja">{{__('challenge2020.yes')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 08 | Robot Dance --}}
            <div class="card scoreform-spacer">
                <div id="M08" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M08-name')}}</div>
                    <div id="M08_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M08-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M08-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M08();" type="radio" id="m08_1_nee" name="M08_1" value="0">
                        <label onclick="js_M08();" for="m08_1_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M08();" type="radio" id="m08_1_ja" name="M08_1" value="1">
                        <label onclick="js_M08();" for="m08_1_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M08-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M08();" type="radio" id="m08_2_nee" name="M08_2" value="0">
                        <label onclick="js_M08();" for="m08_2_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M08();" type="radio" id="m08_2_ja" name="M08_2" value="1">
                        <label onclick="js_M08();" for="m08_2_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M08-scoring3')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M08();" type="radio" id="m08_3_nee" name="M08_3" value="0">
                        <label onclick="js_M08();" for="m08_3_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M08();" type="radio" id="m08_3_ja" name="M08_3" value="1">
                        <label onclick="js_M08();" for="m08_3_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M08-scoring4')}}
                    <div class="fll-beside fll-right">
                        <button onclick="M08_4_add()" onmousedown="M08_4_add_start()" onmouseup="M08_4_add_stop()" class="fll-beside fll-right" type="button">+</button>
                        <div class="fll-beside fll-right" style="width:5px"></div>
                        <input class="fll-beside fll-right" required onchange="js_M08();" onclick="js_M08();" type="number" id="M08_blockAmount" name="M08_4" placeholder="{{__('scoreform-general.amountBricks')}}" max="18" value="">
                        <div class="fll-beside fll-right" style="width:5px"></div>
                        <button onclick="M08_4_deduct()" onmousedown="M08_4_deduct_start()" onmouseup="M08_4_deduct_stop()" class="fll-beside fll-right" type="button">-</button>
                    </div>
                </div>
            </div>

            {{-- Mission 09 | Tire Flip --}}
            <div class="card scoreform-spacer">
                <div id="M09" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M09-name')}}</div>
                    <div id="M09_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M09-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M09-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M09();" type="radio" id="m09_1_geen" name="M09_1" value="0">
                        <label onclick="js_M09();" for="m09_1_geen">{{__('challenge2020.None')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_1_blauw" name="M09_1" value="1">
                        <label onclick="js_M09();" for="m09_1_blauw">{{__('challenge2020.M09-light')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_1_zwart" name="M09_1" value="2">
                        <label onclick="js_M09();" for="m09_1_zwart">{{__('challenge2020.M09-heavy')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_1_beide" name="M09_1" value="3">
                        <label onclick="js_M09();" for="m09_1_beide">{{__('challenge2020.M09-both')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M09-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M09();" type="radio" id="m09_2_geen" name="M09_2" value="0">
                        <label onclick="js_M09();" for="m09_2_geen">{{__('challenge2020.None')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_2_blauw" name="M09_2" value="1">
                        <label onclick="js_M09();" for="m09_2_blauw">{{__('challenge2020.M09-light')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_2_zwart" name="M09_2" value="2">
                        <label onclick="js_M09();" for="m09_2_zwart">{{__('challenge2020.M09-heavy')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_2_beide" name="M09_2" value="3">
                        <label onclick="js_M09();" for="m09_2_beide">{{__('challenge2020.M09-both')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M09-scoring3')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M09();" type="radio" id="m09_3_nee" name="M09_3" value="0">
                        <label onclick="js_M09();" for="m09_3_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M09();" type="radio" id="m09_3_ja" name="M09_3" value="1">
                        <label onclick="js_M09();" for="m09_3_ja">{{__('challenge2020.yes')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 10 | Cell Phone --}}
            <div class="card scoreform-spacer">
                <div id="M10" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M10-name')}}</div>
                    <div id="M10_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M10-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M10-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M10();" type="radio" id="m10_1_nee" name="M10_1" value="0">
                        <label onclick="js_M10();" for="m10_1_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M10();" type="radio" id="m10_1_ja" name="M10_1" value="1">
                        <label onclick="js_M10();" for="m10_1_ja">{{__('challenge2020.yes')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 11 | Treadmill --}}
            <div class="card scoreform-spacer">
                <div id="M11" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M11-name')}}</div>
                    <div id="M11_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M11-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M10-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M11();" type="radio" id="m11_1_geen" name="M11_1" value="0">
                        <label onclick="js_M11();" for="m11_1_geen">{{__('challenge2020.None')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M11();" type="radio" id="m11_1_grijs" name="M11_1" value="1">
                        <label onclick="js_M11();" for="m11_1_grijs">{{__('challenge2020.Gray')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M11();" type="radio" id="m11_1_rood" name="M11_1" value="2">
                        <label onclick="js_M11();" for="m11_1_rood">{{__('challenge2020.Red')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M11();" type="radio" id="m11_1_oranje" name="M11_1" value="3">
                        <label onclick="js_M11();" for="m11_1_oranje">{{__('challenge2020.Orange')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M11();" type="radio" id="m11_1_geel" name="M11_1" value="4">
                        <label onclick="js_M11();" for="m11_1_geel">{{__('challenge2020.Yellow')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M11();" type="radio" id="m11_1_lichtgroen" name="M11_1" value="5">
                        <label onclick="js_M11();" for="m11_1_lichtgroen">{{__('challenge2020.LtGreen')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M11();" type="radio" id="m11_1_donkergroen" name="M11_1" value="6">
                        <label onclick="js_M11();" for="m11_1_donkergroen">{{__('challenge2020.DkGreen')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 12 | Row Machine --}}
            <div class="card scoreform-spacer">
                <div id="M12" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M12-name')}}</div>
                    <div id="M12_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M12-desc')}}</div>

                <div class="card-body radio-toolbar">

                    <div class="alert-danger" id="M12-error"></div>

                    {{__('challenge2020.M12-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M12();" type="radio" id="m12_1_nee" name="M12_1" value="0">
                        <label onclick="js_M12();" for="m12_1_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M12();" type="radio" id="m12_1_ja" name="M12_1" value="1">
                        <label onclick="js_M12();" for="m12_1_ja">{{__('challenge2020.yes')}}</label>
                    </div>
                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M12-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M12();" type="radio" id="m12_2_nee" name="M12_2" value="0">
                        <label onclick="js_M12();" for="m12_2_nee">{{__('challenge2020.no')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M12();" type="radio" id="m12_2_ja" name="M12_2" value="1">
                        <label onclick="js_M12();" for="m12_2_ja">{{__('challenge2020.yes')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 13 | Weight Machine --}}
            <div class="card scoreform-spacer">
                <div id="M13" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M13-name')}}</div>
                    <div id="M13_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M13-desc')}}</div>

                <div class="card-body radio-toolbar">

                    {{__('challenge2020.M13-scoring1')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M13();" type="radio" id="m13_geen" name="M13_1" value="0">
                        <label onclick="js_M13();" for="m13_geen">{{__('challenge2020.None')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M13();" type="radio" id="m13_blauw" name="M13_1" value="1">
                        <label onclick="js_M13();" for="m13_blauw">{{__('challenge2020.Blue')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M13();" type="radio" id="m13_magenta" name="M13_1" value="2">
                        <label onclick="js_M13();" for="m13_magenta">{{__('challenge2020.Magenta')}}</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M13();" type="radio" id="m13_geel" name="M13_1" value="3">
                        <label onclick="js_M13();" for="m13_geel">{{__('challenge2020.Yellow')}}</label>
                    </div>

                </div>
            </div>

            {{-- Mission 14 | Health Units --}}
            <div class="card scoreform-spacer">
                <div id="M14" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.M14-name')}}</div>
                    <div id="M14_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.M14-desc')}}</div>

                <div class="card-body radio-toolbar">

                    <div class="alert-danger" id="M14-error"></div>

                    {{__('challenge2020.M14-scoring1')}}
                    <div class="fll-beside fll-right">
                        <button onclick="M14_1_add()"   onmousedown="M14_1_add_start()" onmouseup="M14_1_add_stop()"class="fll-beside fll-right" type="button">+</button>
                        <div class="fll-beside fll-right" style="width:5px"></div>
                        <input class="fll-beside fll-right" required onchange="js_M14();" onclick="js_M14();" type="number" id="M14_healthpoints" name="M14_1" placeholder="{{__("challenge2020.amountHealthunits")}}" max="8" value="">
                        <div class="fll-beside fll-right" style="width:5px"></div>
                        <button onclick="M14_1_deduct()" onmousedown="M14_1_deduct_start()" onmouseup="M14_1_deduct_stop()"class="fll-beside fll-right" type="button">-</button>
                    </div>

                    <div class="fll-spacer"></div>

                    {{__('challenge2020.M14-scoring2')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M14();" type="radio" id="m14_0" name="M14_2" value="0">
                        <label onclick="js_M14();" for="m14_0">0</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M14();" type="radio" id="m14_1" name="M14_2" value="1">
                        <label onclick="js_M14();" for="m14_1">1</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M14();" type="radio" id="m14_2" name="M14_2" value="2">
                        <label onclick="js_M14();" for="m14_2">2</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M14();" type="radio" id="m14_3" name="M14_2" value="3">
                        <label onclick="js_M14();" for="m14_3">3</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M14();" type="radio" id="m14_4" name="M14_2" value="4">
                        <label onclick="js_M14();" for="m14_4">4</label>
                    </div>

                </div>
            </div>

            {{-- Mission 15 | Precision --}}
            <div class="card scoreform-spacer">
                <div id="M15" class="card-header">
                    <div class="fll-beside">{{__('challenge2020.precision-name')}}</div>
                    <div id="M15_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body">{{__('challenge2020.precision-desc')}}</div>

                <div class="card-body radio-toolbar">
                    {{__('challenge2020.precision-scoring')}}
                    <div class="fll-beside fll-right">
                        <input required onclick="js_M15();" type="radio" id="m15_0" name="M15_1" value="0">
                        <label onclick="js_M15();" for="m15_0">0</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M15();" type="radio" id="m15_1" name="M15_1" value="1">
                        <label onclick="js_M15();" for="m15_1">1</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M15();" type="radio" id="m15_2" name="M15_1" value="2">
                        <label onclick="js_M15();" for="m15_2">2</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M15();" type="radio" id="m15_3" name="M15_1" value="3">
                        <label onclick="js_M15();" for="m15_3">3</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M15();" type="radio" id="m15_4" name="M15_1" value="4">
                        <label onclick="js_M15();" for="m15_4">4</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M15();" type="radio" id="m15_5" name="M15_1" value="5">
                        <label onclick="js_M15();" for="m15_5">5</label>
                        <div class="fll-beside" style="width: 20px"></div>
                        <input required onclick="js_M15();" type="radio" id="m15_6" name="M15_1" value="6">
                        <label onclick="js_M15();" for="m15_6">6</label>
                    </div>

                </div>
            </div>

            @can('scoreform.save')
            {{-- Remarks --}}
            <div class="card scoreform-spacer">
                <div id="notition" class="card-header">
                    <div class="fll-beside">{{__('scoreform-general.remarks')}}</div>
                </div>

                <div class="card-body r">
                    <textarea rows="3" cols="100" id="notition" name="notition" placeholder="{{__('scoreform-general.remarks')}}?">

                    </textarea>
                </div>
            </div>
            @endcan

            {{-- Submit --}}
            <div class="card scoreform-spacer">
                <div id="score" class="card-header">
                    <div class="fll-beside">{{__('scoreform-general.submit')}}</div>
                    <div id="total_pt" class="fll-beside fll-right">0 pt</div>
                </div>

                <div class="card-body radio-toolbar">
                    <input type="button" onclick="calcScore()" value="{{__('scoreform-general.checkForm')}}" >
                </div>



                @can('scoreform.save')
                <div class="card-body radio-toolbar">
                    <input type="submit" value="{{__('scoreform-general.submit')}}" id="submitButton" disabled>
                </div>
                @endcan
            </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/challenge2020.js') }}" defer></script>
@endsection
