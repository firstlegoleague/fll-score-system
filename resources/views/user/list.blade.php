@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('users.title')}}</div>

                <div class="card-body">
                    <table
                        data-toggle="table"
                        data-search="true">
                        <thead>
                            <tr>
                                <th>{{__('general.id')}}</th>
                                <th>{{__('general.name')}}</th>
                                <th>{{__('general.email')}}</th>
                                <th>{{__('users.role')}}</th>
                                <th>{{__('general.edit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr
                                @if($user->hasRole('super-adminq') or $user->hasRole('nationalorganization'))
                                style="display: none"
                                @endif
                            >
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->roles->first()->name}}</td>
                                <td>

                                @if(!$user->hasRole('super-admin'))
                                    <a href="{{route('users.edit', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$user->id])}}" class="btn btn-default btn-info">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
