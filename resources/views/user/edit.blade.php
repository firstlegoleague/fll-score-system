@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{__('users.edit')}}</div>

                <div class="card-body">
                    <form autocomplete="off" method="POST" action="{{route('users.save', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}">
                        @csrf

                        <input type="hidden" id="id" name="id" value="{{$user->id}}">

                        {{-- user email --}}
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('general.email') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" required value="{{$user->email}}">
                            </div>
                        </div>

                        {{-- user email --}}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('general.name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required value="{{$user->name}}">
                            </div>
                        </div>

                        {{-- Role --}}
                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('users.role') }}</label>
                            <div class="col-md-6">
                                <select name="role" id="role" class="form-control" required>
                                    @foreach($roles as $role)
                                        <option @if($role->id == $user->roles->first()->id) selected @endif value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('auth.changePassword') }}</label>
                            <div class="col-md-6">

                                <a class="btn btn-warning" href="/users/{{str_replace('_', '-', app()->getLocale())}}/force/{{$user->id}}">{{__('auth.changePassword')}}</a>

                            </div>
                        </div>




                        <button type="submit" class="btn btn-primary">
                            {{ __('settings.submit') }}
                        </button>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
