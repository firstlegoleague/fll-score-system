@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{__('account.forcePassword')}}</div>
                    <div class="card-body">
                        <form autocomplete="off" method="POST" action="{{route('users.forcePost', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$user->id])}}">
                            @csrf

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('account.name') }}</label>
                                <div class="col-md-6">

                                    <label class="col-form-label text-md-left">{{ $user->name }}</label>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="newPassword" class="col-md-4 col-form-label text-md-right">{{ __('auth.password') }}</label>
                                <div class="col-md-6">
                                    <input id="newPassword" type="text" class="form-control" name="newPassword" required>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">
                                {{ __('settings.submit') }}
                            </button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
