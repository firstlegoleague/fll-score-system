@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('account.title')}}</div>

                <div class="card-body">
                   <table class="table table-striped">
                       <tr>
                           <td>{{__('users.id')}}</td>
                           <td>{{$user->id}}</td>
                       </tr>
                       <tr>
                           <td>{{__('account.name')}}</td>
                           <td>{{$user->name}}</td>
                       </tr>
                       <tr>
                           <td>{{__('general.email')}}</td>
                           <td>{{$user->email}}</td>
                       </tr>
                       <tr>
                           <td>{{__('users.role')}}</td>
                           <td>{{$user->roles->first()->name}}</td>
                       </tr>
                       <tr>
                           <td>
                               <form action="{{ route('account.changePassword', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                   <input class="btn btn-primary" type="submit" value="{{__('account.changepassword')}}" />
                               </form>


                           </td>
                           <td></td>
                       </tr>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
