@extends('layouts.screen-db')

@section('content')
    <!-- This is an error-message for small screens -->
    <div class="d-lg-none d-xl-none col-12">
        <div class="alert-danger align-middle">
            <p class="text-center">Your screen is too small to display this page!</p>
        </div>
    </div>

    <!-- Contents for larger screens -->
    <div class="d-none d-lg-block d-xl-block">
        <!-- Loading screen -->
    {{--        <div class="spinner-border text-info spinner" role="status" id="overlay_load">--}}
    {{--        </div>--}}

    <!-- The content -->
        <div id="overlay_content" onload="pageScroll();">
            <div class="row justify-content-start">
                <div class="col-12 border-info border text-center" id="title-bar">
                    <h1>
                        @if(\App\settings::getFinalName() == "NotSet")
                            {{__('general.results')}}
                        @else
                            {{__('general.results')}} | {{\App\settings::getFinalName()}}
                        @endif
                        @if(\App\Rounds::all()->where('public', 1)->count() != 0)
                            | {{\App\Rounds::all()->where('public', 1)->sortByDesc('id')->first()->round}}
                        @endif
                    </h1>
                </div>
            </div>

            <div class="row justify-content-center mt-5 mb-5">


                <div class="col-8 border border-primary" id="content-screen">
                    <table id="myTable">
                        @foreach($games as $game)
                            <tr>
                                <td>
                                    {{ $loop->index }}
                                </td>
                                <td>
                                    {{\App\Teams::all()->where('id', $game->teamID)->first()->teamNumber}} {{\App\Teams::all()->where('id', $game->teamID)->first()->teamname}}
                                </td>
                                <td>
                                    {{\App\Rounds::all()->where('id', $game->roundID)->first()->round}}
                                </td>
                                <td>
                                    {{$game->totalScore}}
                                </td>

                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
