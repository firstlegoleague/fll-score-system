@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        {{__('general.results')}}
                        @if(\App\settings::getFinalName() != "NotSet")
                            | {{\App\settings::getFinalName()}}
                        @endif
                        @if(\App\Rounds::all()->where('public', 1)->count() != 0)
                            | {{\App\Rounds::all()->where('public', 1)->sortByDesc('id')->first()->round}}
                        @endif
                    </div>
                    <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th> {{__('general.rank')}}</th>
                            <th> {{__('teams.number')}}</th>
                            <th> {{__('teams.name')}}</th>
                            <th> {{__('rounds.round')}}</th>
                            <th> {{__('general.score')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($games as $game)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }} {{-- No! This is not off by one! Arrays start at zero. We are techical people right? --}}
                                </td>
                                <td>
                                    {{\App\Teams::all()->where('id', $game->teamID)->first()->teamNumber}}
                                </td>
                                <td>
                                    {{\App\Teams::all()->where('id', $game->teamID)->first()->teamname}}
                                </td>
                                <td>
                                    {{\App\Rounds::all()->where('id', $game->roundID)->first()->round}}
                                </td>
                                <td>
                                    {{$game->totalScore}}
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
