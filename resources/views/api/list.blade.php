@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><div class="fll-beside">API list</div></div>

                    <div class="card-body">

                        <table
                            data-toggle="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>API key</th>
                                <th>User</th>
                                <th>Status</th>
                                <th>Disable</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($apikeys as $key)
                                <tr>
                                    <td>{{$key->id}}</td>
                                    <td>{{$key->apikey}}</td>
                                    <td>{{$key->userID}}</td>
                                    <td>{{$key->enabled}}</td>
                                    <td>Nope</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
