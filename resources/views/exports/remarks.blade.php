<table>
    <thead>
    <tr>
        <th>{{__('teams.number')}}</th>
        <th>{{__('teams.name')}}</th>
        <th>{{__('rounds.round')}}</th>
        <th>{{__('general.score')}}</th>
        <th>{{__('scoreform-general.remarks')}}</th>

    </tr>
    </thead>
    <tbody>
    @foreach($remarks as $remark)
        <tr>
            <td>{{\App\Teams::all()->where('id', $remark->teamID)->first()->teamNumber}}</td>
            <td>{{\App\Teams::all()->where('id', $remark->teamID)->first()->teamname}}</td>
            <td>{{\App\Rounds::all()->where('id', $remark->roundID)->first()->round}}</td>
            <td>{{$remark->totalScore}}</td>
            <td>{{$remark->remarks}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
