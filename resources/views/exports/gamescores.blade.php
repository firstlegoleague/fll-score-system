<table>
    <thead>
    <tr>
        <th>{{__('teams.number')}}</th>
        <th>{{__('teams.name')}}</th>
        @foreach($rounds as $round)
            <th>{{$round->round}}</th>
        @endforeach
        @foreach($rounds as $round)
            <th>GP - {{$round->round}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($teams as $team)
        <tr>
            <td>{{$team->teamNumber}}</td>
            <td>{{$team->teamname}}</td>
            @foreach($rounds as $round)
                <td>{{  \App\ResultsChecker::getResult($team->id, $round->id) }}</td>
            @endforeach
            @foreach($rounds as $round)
                <td>{{  \App\ResultsChecker::getResultGP($team->id, $round->id) }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
