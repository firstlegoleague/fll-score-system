@extends('layouts.app')

@section('content')
<div class="container">
    @if (session()->has('teamsaved'))
        <div class="alert alert-success" role="alert">
            {{__('teams.savedsuccess')}}
        </div>
    @endif

    @if (session()->has('teamdeleted'))
        <div class="alert alert-success" role="alert">
            {{__('teams.deletesuccess')}}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><div class="fll-beside">{{__('teams.list')}}</div>
                    <div class="fll-right fll-beside"> <a href="/teams/{{ str_replace('_', '-', app()->getLocale()) }}/add/" class="btn btn-default btn-success"> <i class="fas fa-plus-circle"></i> {{__('general.add')}} </a></div> </div>

                <div class="card-body">

                    <table
                        data-toggle="table"
                        data-search="true">
                        <thead>
                            <tr>
                                <th data-sortable="true">{{__('teams.ID')}}</th>
                                <th data-sortable="true">{{__('teams.number')}}</th>
                                <th data-sortable="true">{{__('teams.name')}}</th>
                                <th data-sortable="true">{{__('teams.affiliate')}}</th>
                                <th>{{__('teams.edit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($teams as $team)
                            <tr>
                                <td>{{$team->id}}</td>
                                <td>{{$team->teamNumber}}</td>
                                <td>{{$team->teamname}}</td>
                                <td>{{$team->affiliate}}</td>
                                <td>

                                    <a href="/teams/{{ app()->getLocale() }}/edit/{{$team->id}}" class="btn btn-default btn-info">
                                        <i class="fas fa-eye"></i>
                                    </a>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
