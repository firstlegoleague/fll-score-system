@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('teams.add')}}</div>

                <div class="card-body">
                    <form autocomplete="off" method="POST" action="/teams/{{ str_replace('_', '-', app()->getLocale()) }}/add">
                        @csrf

                        {{-- Team Number --}}
                        <div class="form-group row">
                            <label for="teamnumber" class="col-md-4 col-form-label text-md-right">{{ __('teams.number') }}</label>
                            <div class="col-md-6">
                                <input id="teamnumber" type="text" class="form-control" name="teamnumber" required>
                            </div>
                        </div>

                        {{-- Team name --}}
                        <div class="form-group row">
                            <label for="teamname" class="col-md-4 col-form-label text-md-right">{{ __('teams.name') }}</label>
                            <div class="col-md-6">
                                <input id="teamname" type="text" class="form-control" name="teamname" required>
                            </div>
                        </div>

                        {{-- Affiliate --}}
                        <div class="form-group row">
                            <label for="affiliate" class="col-md-4 col-form-label text-md-right">{{ __('teams.affiliate') }}</label>
                            <div class="col-md-6">
                                <input id="affiliate" type="text" class="form-control" name="affiliate">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            {{ __('settings.submit') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
