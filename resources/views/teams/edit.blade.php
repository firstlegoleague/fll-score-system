@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('teams.add')}}</div>

                <div class="card-body">
                    <form autocomplete="off" method="POST" action="{{route('teams.save',['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$team->id])}}">
                        @csrf

                        {{-- Team Number --}}
                        <div class="form-group row">
                            <label for="teamnumber" class="col-md-4 col-form-label text-md-right">{{ __('teams.number') }}</label>
                            <div class="col-md-6">
                                <input readonly id="teamnumber" type="text" class="form-control" name="teamnumber" requireda value="{{$team->teamNumber}}">
                            </div>
                        </div>

                        {{-- Team name --}}
                        <div class="form-group row">
                            <label for="teamname" class="col-md-4 col-form-label text-md-right">{{ __('teams.name') }}</label>
                            <div class="col-md-6">
                                <input id="teamname" type="text" class="form-control" name="teamname" required value="{{$team->teamname}}">
                            </div>
                        </div>

                        {{-- Affiliate --}}
                        <div class="form-group row">
                            <label for="affiliate" class="col-md-4 col-form-label text-md-right">{{ __('teams.affiliate') }}</label>
                            <div class="col-md-6">
                                <input id="affiliate" type="text" class="form-control" name="affiliate" value="{{$team->affiliate}}">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            {{ __('general.save') }}
                        </button>
                    </form>

                    <div style="margin-top: 20px"></div>
                    <form autocomplete="off" method="post" action="{{route('teams.delete',['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$team->id])}}">
                    @csrf
                        <button
                            @if(\App\Teams::hasResults($team->id) == 1)
                                disabled
                            @endif

                            type="submit" class="btn btn-danger">
                            {{ __('general.delete') }}
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
