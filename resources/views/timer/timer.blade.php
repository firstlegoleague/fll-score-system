@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('timer.timer')}}</div>

                    <div class="card-body">
                        <div class="timer">
                            <div class="fll-beside">0</div><div class="fll-beside" id="timer_min">2</div><div class="fll-beside">:</div><div class="fll-beside zerohide" id="timer_zero">0</div><div class="fll-beside" id="timer_sec">30</div>
                        </div>
                    </div>
                </div>

                <div class="fll-spacer"></div>
                <div class="fll-spacer"></div>
                <div class="fll-spacer"></div>

                <div class="card">
                    <div class="card-header">{{__('timer.timer-control')}}</div>

                    <div class="card-body">
                        <div class="fll-beside">
                            <button onclick="timer_start()" class="alert alert-success">{{__('timer.start')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="timer_reset()" class="alert alert-info">{{__('timer.reset')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="timer_stop()" class="alert alert-danger">{{__('timer.stop')}}</button>
                        </div>
                    </div>
                </div>

                <div class="fll-spacer"></div>
                <div class="fll-spacer"></div>
                <div class="fll-spacer"></div>


                <button type="button" class="card-header collapsible">{{__('timer.test')}}</button>
                <div class="card content" style="display:none">
                    {{--                    <div class="card-header">Test the sounds</div>--}}

                    <div class="card-body">
                        <div class="fll-beside">
                            <button onclick="timer_test_start()" class="alert alert-success">{{__('timer.start')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="timer_test_end()" class="alert alert-info">{{__('timer.reset')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="timer_test_stop()" class="alert alert-danger">{{__('timer.stop')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
