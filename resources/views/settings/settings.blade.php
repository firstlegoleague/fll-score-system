@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{__('settings.title')}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form autocomplete="off" method="POST" action="/settings/{{ str_replace('_', '-', app()->getLocale()) }}">
                        @csrf
                        {{-- Final name --}}
                        <div class="form-group row">
                            <label for="finalname" class="col-md-4 col-form-label text-md-right">{{ __('settings.finalname') }}</label>
                            <div class="col-md-6">
                                <input id="finalname" type="text" class="form-control" name="finalname" value="{{ App\settings::all()->where('key', 'finalName')->first()->value }}" required>
                            </div>
                        </div>

                        {{-- Season --}}
                        <div class="form-group row">
                            <label for="season" class="col-md-4 col-form-label text-md-right">{{ __('settings.season') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="season" id="season">
{{--                                    <option value="2019" @if( App\settings::all()->where('key', 'season')->first()->value == "2019") selected @endif>2019 | City Shaper (Not implemented yet)</option>--}}
                                    <option value="2020" @if( App\settings::all()->where('key', 'season')->first()->value == "2020") selected @endif>2020 | {{__('seasons.2020')}}</option>
                                    <option value="2021" @if( App\settings::all()->where('key', 'season')->first()->value == "2021") selected @endif>2021 | {{__('seasons.2021')}}</option>
{{--                                    <option value="2022" @if( App\settings::all()->where('key', 'season')->first()->value == "2022") selected @endif>2022 | </option>--}}
{{--                                    <option value="2023" @if( App\settings::all()->where('key', 'season')->first()->value == "2023") selected @endif>2023 | </option>--}}
{{--                                    <option value="2024" @if( App\settings::all()->where('key', 'season')->first()->value == "2024") selected @endif>2024 | </option>--}}
{{--                                    <option value="2025" @if( App\settings::all()->where('key', 'season')->first()->value == "2025") selected @endif>2025 | </option>--}}
{{--                                    <option value="2026" @if( App\settings::all()->where('key', 'season')->first()->value == "2026") selected @endif>2026 | </option>--}}
                                </select>
                            </div>
                        </div>

                        {{-- Register enabled --}}
                        <div class="form-group row">
                            <label for="registerEnabled" class="col-md-4 col-form-label text-md-right">{{ __('settings.registerEnabled') }}</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="registerEnabled" name="registerEnabled" @if(App\settings::all()->where('key', 'register')->first()->value == "1") checked @endif class="form-control">
                            </div>
                        </div>

{{--                         Score public --}}
                        <div class="form-group row">
                            <label for="scorePublic" class="col-md-4 col-form-label text-md-right">{{ __('settings.scorePublic') }}</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="scorePublic" name="scorePublic" @if(App\settings::all()->where('key', 'gamePublic')->first()->value == "1") checked @endif class="form-control">
                            </div>
                        </div>

                        {{-- Submit button --}}
                        <button type="submit" class="btn btn-primary">
                            {{ __('settings.submit') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
