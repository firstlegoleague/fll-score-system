<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FLL OVERLAY SYSTEM') }}</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('img/favicon.png') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">


    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="https://kit.fontawesome.com/b289b4ce85.js" crossorigin="anonymous"></script>

    {{--    For the search dropdown--}}
    <script src="{{ asset('js/general.js') }}" defer></script>
{{--    <script src="{{ asset('js/screen.js') }}" defer></script>--}}
{{--    <script src="{{ asset('js/overlay/screen.js') }}" defer></script>--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"; rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js";></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/fll.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/screen.css') }}" rel="stylesheet">


</head>
<body>
<div id="app">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>

<footer class="fll-footer">

    <div class="fll-footer-content">
        {{--            <div class="fll-beside" style="font-weight: bold;font-size: 150%;">{{__('general.footer-volunteer')}}</div>--}}
        {{--            <div class="fll-beside" style="width: 10%"></div>--}}
        <img class="fll-footer-logo" src="{{ asset('img/logos/fll.png') }}">
        <div class="fll-beside" style="width: 10%"></div>
        <img class="fll-footer-logo" src="{{ asset('img/logos/stp.png') }}">
    </div>

</footer>

</body>
{{--<script src="{{ asset('js/overlay/scrolling.js') }}"></script>--}}
</html>
