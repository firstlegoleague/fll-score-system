<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon/favicon.png') }}">


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/general.js') }}" defer></script>
{{--    <script src="{{ asset('js/fll.js') }}" defer></script>--}}
    <script src="{{ asset('js/timer.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/b289b4ce85.js" crossorigin="anonymous"></script>

    {{--    For the search dropdown--}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/fll.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/timer.css') }}" rel="stylesheet">


</head>
<body style="background-color: #ffffe6"></body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-fll">
        <div class="container">
            @guest()
                <a class="navbar-brand-fll navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'FLL Scoring') }}
                    @if(\App\settings::all()->where('key','finalName')->first()->value != "NotSet")
                        | {{\App\settings::all()->where('key','finalName')->first()->value}}
                    @endif
                </a>
            @else
                <a class="navbar-brand-fll navbar-brand" href="/home/{{ str_replace('_', '-', app()->getLocale()) }}">
                    {{ config('app.name', 'FLL Scoring') }}
                    @if(\App\settings::all()->where('key','finalName')->first()->value != "NotSet")
                        | {{\App\settings::all()->where('key','finalName')->first()->value}}
                    @endif
                </a>
            @endguest

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon">
                    <i class="fa fa-navicon" style="font-size:20px;"></i>
                </span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Menu <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="/challenge/{{ str_replace('_', '-', app()->getLocale()) }}/{{\App\settings::all()->where('key', 'season')->first()->value }}">
                                    {{__('menu.newForm')}}
                                </a>

                                @if(\App\Settings::getGamePublic() == 1)
                                    <a class="dropdown-item" href="{{ route('scoreboard.public', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}">
                                        {{__('menu.scoreboard-public')}}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('overview.gametablepublic', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}">
                                        {{__('menu.overview-gametable')}}
                                    </a>
                                @endif

                                <a class="dropdown-item" href="{{ route('login') }}">
                                    {{__('menu.login')}}
                                </a>
                            </div>
                        </li>

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">


                                    <a class="dropdown-item" href="/challenge/{{ str_replace('_', '-', app()->getLocale()) }}/{{\App\settings::all()->where('key', 'season')->first()->value }}">
                                        {{__('menu.newForm')}}
                                    </a>

                                    <a class="dropdown-item" href="{{route('timer')}}">
                                        {{__('menu.timer')}}
                                    </a>

                                @can('teams.add')
                                    <a class="dropdown-item" href="{{ route('teams.list', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                        {{__('menu.teams')}}
                                    </a>
                                @endcan

                                @can('rounds.add')
                                    <a class="dropdown-item" href="{{ route('rounds.list', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                        {{__('menu.rounds')}}
                                    </a>
                                @endcan

                                @can('rounds.add')
                                    <a class="dropdown-item" href="{{ route('scoreboard.public', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                        {{__('menu.scoreboard-public')}}
                                    </a>
                                @endcan

                                @can('users.add')
                                    <a class="dropdown-item" href="{{ route('users.list', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                        {{__('menu.users')}}
                                    </a>
                                @endcan

                                @can('rounds.view')
                                    <a class="dropdown-item" href="{{ route('overview.gametable', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                        {{__('menu.overview-gametable')}}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('games.list', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                        {{__('menu.gamelist')}}
                                    </a>
                                @endcan

                                @can('settings.change')
                                    <a class="dropdown-item" href="/settings/{{ str_replace('_', '-', app()->getLocale()) }}">
                                        {{__('menu.settings')}}
                                    </a>
                                @endcan

                                <a class="dropdown-item" href="{{ route('account.view', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                    {{__('menu.account')}}
                                </a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{__('menu.logout')}}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        <svg viewBox="0 0 100 60" preserveAspectRatio="none" height="60%" width="100%" style="position: fixed; bottom: 0px;"><polygon fill="#c61e21" points="0 15,100 0, 100 60, 0 60"></polygon></svg>
        <svg viewBox="0 0 100 50" preserveAspectRatio="none" height="50%" width="100%" style="position: fixed; bottom: 0px;"><polygon fill="#205886" points="0 0,100 10, 100 50, 0 50"></polygon></svg>
        @yield('content')
        <div style="height: 100px"></div>


    </main>

    <footer class="fll-footer">

        <div class="fll-footer-content">
{{--            <div class="fll-beside" style="font-weight: bold;font-size: 150%;">{{__('general.footer-partners')}}</div>--}}
{{--            <div class="fll-beside" style="width: 10%"></div>--}}
            <div class="fll-beside" style="font-weight: bold;font-size: 150%;">{{__('general.footer-volunteer')}}</div>
{{--            <img class="fll-footer-logo" src="{{ asset('img/logos/fll.png') }}">--}}
{{--            <div class="fll-beside" style="width: 10%"></div>--}}
{{--            <img class="fll-footer-logo" src="{{ asset('img/logos/stp.png') }}">--}}
        </div>

    </footer>



</div>

</body>

</html>
