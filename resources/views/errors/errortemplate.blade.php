<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Scoringtool') }}</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon/favicon.png') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/fll.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('css/timer.css') }}" rel="stylesheet">--}}


</head>
<body style="background-color: #ffffe6"></body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-fll">
        <div class="container">
            <div class="navbar-brand-fll navbar-brand">FLL Scoring</div>
        </div>
    </nav>

    <main class="py-4">
        <svg viewBox="0 0 100 60" preserveAspectRatio="none" height="60%" width="100%" style="position: fixed; bottom: 0px;"><polygon fill="#c61e21" points="0 15,100 0, 100 60, 0 60"></polygon></svg>
        <svg viewBox="0 0 100 50" preserveAspectRatio="none" height="50%" width="100%" style="position: fixed; bottom: 0px;"><polygon fill="#205886" points="0 0,100 10, 100 50, 0 50"></polygon></svg>
        @yield('content')
        <div style="height: 100px"></div>
    </main>

    <footer class="fll-footer">

        <div class="fll-footer-content">
            <div class="fll-beside" style="font-weight: bold;font-size: 150%;">{{__('general.footer-volunteer')}}</div>
{{--            <div class="fll-beside" style="width: 10%"></div>--}}
{{--            <img class="fll-footer-logo" src="{{ asset('img/logos/fll.png') }}">--}}
{{--            <div class="fll-beside" style="width: 10%"></div>--}}
{{--            <img class="fll-footer-logo" src="{{ asset('img/logos/stp.png') }}">--}}
        </div>

    </footer>



</div>

</body>

</html>
