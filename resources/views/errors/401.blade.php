@extends('errors::errortemplate')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        401 {{__('Unauthorized')}}
                    </div>

                    <div class="card-body">
                        <h3 style="text-align: center;" >{{__('errors.401_title')}}</h3>
                        <img class="fll-center-emmit" src="{{ asset('img/404-image.png') }}">
                        <h2 style="text-align: center;">{{__('errors.401_subtitle')}}</h2>
                        <div style="text-align: center;">{{__('errors.401_text')}}</div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
