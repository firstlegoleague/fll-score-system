@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><div class="fll-beside">{{__('overview.gameTitle')}}</div></div>

                <div class="card-body">

                    <table
                        data-toggle="table"
                        data-search="true">
                        <thead>
                            <tr>
                                <th data-sortable="true">{{__('teams.number')}}</th>
                                <th data-sortable="true">{{__('teams.name')}}</th>
                                @foreach($rounds as $round)
                                <th data-sortable="true">{{$round->round}}</th>
                                @endforeach
{{--                                <th>{{__('overview.complete')}}</th>--}}
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($teams as $team)
                            <tr>
                                <td>{{$team->teamNumber}}</td>
                                <td>{{$team->teamname}}</td>

                                @foreach($rounds as $round)
                                    <td class="
                                    @if(\App\ResultsChecker::getResult($team->id, $round->id) == -1) alert-danger
                                    @elseif(\App\ResultsChecker::getResult($team->id, $round->id) == 0)
                                    @else
                                        alert-success
                                    @endif

                                    ">{{  \App\ResultsChecker::getResult($team->id, $round->id) }}</td>
                                @endforeach
{{--                                <td>@if(\App\ResultsChecker::getAllResults($team->id) == 3) Y @else N @endif</td>--}}
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
