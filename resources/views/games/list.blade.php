@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="fll-beside">{{__('games.title')}}</div>
                        <div class="fll-right fll-beside">
                           <form method="get" action="{{route('export.gamescores')}}">
                                <button type="submit" class="btn btn-success">
                                    <i class="fas fa-download"></i> {{__('general.downloadScores')}}
                                </button>
                           </form>
                        </div>
                        <div style="margin-right: 20px" class="fll-right fll-beside">
                            <form method="get" action="{{route('export.remarks')}}">
                                <button type="submit" class="btn btn-success">
                                    <i class="fas fa-download"></i> {{__('general.downloadRemarks')}}
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class="card-body">
                        <table
                            data-toggle="table"
                            data-search="true">
                            <thead>
                            <tr>
                                <th data-sortable="true">{{__('games.id')}}</th>
                                <th data-sortable="true">{{__('teams.number')}}</th>
                                <th data-sortable="true">{{__('teams.name')}}</th>
                                <th data-sortable="true">{{__('rounds.round')}}</th>
                                <th data-sortable="true">{{__('games.score')}}</th>
                                <th data-sortable="true">{{__('scoreform-general.remarks')}}</th>
                                <th>{{__('general.edit')}}</th>
                                <th>{{__('general.delete')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($games as $game)
                                <tr>
                                    <td>{{$game->id}}</td>
                                    <td>{{ \App\Teams::all()->where('id', $game->teamID)->first()->teamNumber}}</td>
                                    <td>{{ \App\Teams::all()->where('id', $game->teamID)->first()->teamname}}</td>
                                    <td>{{ \App\Rounds::all()->where('id', $game->roundID)->first()->round }}</td>
                                    <td>{{ $game->totalScore }}</td>
                                    <td>
                                        {{$game->remarks}}

                                    </td>

                                    <td>
                                        @can("scoreform.view.others")
                                        <a href="{{route('games.edit', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$game->id])}}"
                                           class="btn btn-default btn-info">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can("scoreform.override")
                                            <a href="{{route('games.delete', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$game->id])}}"
                                               class="btn btn-danger btn-info">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
