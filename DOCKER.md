# Docker for FLL scoring
Its build with docker compose, a stack of the app it self, a db, and a nginx container.

## Env file
At first, change your env file. This will be needed for the internals

## Build the docker
- `docker-compose build`
- `docker-compose up -d`
- `./dockers-after.sh`

Then on port 8080 the application should run.