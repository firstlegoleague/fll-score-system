<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BasicUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create an Administrator account
        $user = new App\User();
        $user->name = "Administrator";
        $user->email = "admin@fllscoring.nl";
        $user->password = Hash::make(env("ADMIN_PASSWORD", "password"));
        $user->save();

        // Give admin the super admin rights
        $role = Role::findByName('super-admin');
        $role->users()->attach($user);


        $nfo = new App\User();
        $nfo->name = "Stitching Techniek Promotie";
        $nfo->email = "stp@fllscoring.nl";
        $nfo->password = Hash::make(env('STP_PASSWORD', "nationalOrganizer"));
        $nfo->save();

        $role_nfo = Role::findByName("nationalorganization");
        $role_nfo->users()->attach($nfo);

        $ref = new App\User();
        $ref->name = "Scheidsrechter";
        $ref->email = "ref@fllscoring.nl";
        $ref->password = Hash::make("wachtwoord");
        $ref->save();

        $role_ref = Role::findByName("referee");
        $role_ref->users()->attach($ref);


        $local = new App\User();
        $local->name = "Organisatie";
        $local->email = "organisatie@fllscoring.nl";
        $local->password = Hash::make("ChangeMe");
        $local->save();

        $local_role = Role::findByName("localorganization");
        $local_role->users()->attach($local);

    }
}
