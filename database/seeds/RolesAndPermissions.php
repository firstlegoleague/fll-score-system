<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name'=>'scoreform.*']);
        Permission::create(['name'=>'scoreform.save']);
        Permission::create(['name'=>'scoreform.view.own']);
        Permission::create(['name'=>'scoreform.view.others']);
        Permission::create(['name'=>'scoreform.view.scores']);
        Permission::create(['name'=>'scoreform.override']);
        Permission::create(['name'=>'scoreform.delete']);

        Permission::create(['name'=>'judgeform.*']);
        Permission::create(['name'=>'judgeform.save']);
        Permission::create(['name'=>'judgeform.view.own']);
        Permission::create(['name'=>'judgeform.view.others']);
        Permission::create(['name'=>'judgeform.view.scores']);
        Permission::create(['name'=>'judgeform.override']);
        Permission::create(['name'=>'judgeform.delete']);

        Permission::create(['name'=>'teams.*']);
        Permission::create(['name'=>'teams.view']);
        Permission::create(['name'=>'teams.add']);
        Permission::create(['name'=>'teams.edit']);
        Permission::create(['name'=>'teams.delete']);

        Permission::create(['name'=>'rounds.*']);
        Permission::create(['name'=>'rounds.view']);
        Permission::create(['name'=>'rounds.add']);
        Permission::create(['name'=>'rounds.edit']);
        Permission::create(['name'=>'rounds.delete']);

        Permission::create(['name'=>'users.*']);
        Permission::create(['name'=>'users.view']);
        Permission::create(['name'=>'users.add']);
        Permission::create(['name'=>'users.edit']);
        Permission::create(['name'=>'users.delete']);
        Permission::create(['name'=>'users.changeRole']);

        Permission::create(['name'=>'settings.change']);

        Role::create(['name'=>'admin']);
        Role::create(['name'=>'nationalorganization']);
        Role::create(['name'=>'localorganization']);
        Role::create(['name'=>'secretariat']);
        Role::create(['name'=>'headreferee']);
        Role::create(['name'=>'headjudge']);
        Role::create(['name'=>'judge']);
        Role::create(['name'=>'referee']);
        Role::create(['name'=>'viewer']);
        Role::create(['name'=>'guest']);

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        Role::findByName('admin')->givePermissionTo([
            'scoreform.*',
            'teams.*',
            'rounds.*',
            'judgeform.*',
            'users.*',
            'settings.change'
        ]);

        Role::findByName('nationalorganization')->givePermissionTo([
            'scoreform.*',
            'teams.*',
            'rounds.*',
            'judgeform.*',
            'users.*',
            'settings.change'
        ]);

        Role::findByName('localorganization')->givePermissionTo([
            'scoreform.*',
            'teams.*',
            'rounds.*',
            'judgeform.*',
            'users.*',
            'settings.change'
        ]);

        Role::findByName('secretariat')->givePermissionTo([
            'scoreform.*',
            'teams.*',
            'rounds.*',
            'judgeform.*',
            'users.*'
        ]);

        Role::findByName('headreferee')->givePermissionTo([
            'scoreform.save',
            'scoreform.view.own',
            'scoreform.view.others',
            'teams.view',
        ]);

        Role::findByName('headjudge')->givePermissionTo([
            'judgeform.save',
            'judgeform.view.own',
            'judgeform.view.others',
            'teams.view',
        ]);

        Role::findByName('referee')->givePermissionTo([
            'scoreform.save',
            'scoreform.view.own',
            'teams.view',
        ]);

        Role::findByName('judge')->givePermissionTo([
            'judgeform.save',
            'judgeform.view.own',
            'teams.view',
        ]);
    }
}
