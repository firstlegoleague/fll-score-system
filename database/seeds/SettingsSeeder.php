<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => "finalName",
            'value' => "NotSet",
        ]);

        DB::table('settings')->insert([
            'key' => "season",
            'value' => "2021",
        ]);

        DB::table('settings')->insert([
            'key' => "register",
            'value' => "1",
        ]);

        DB::table('settings')->insert([
            'key' => "gamePublic",
            'value' => "0",
        ]);
    }
}
