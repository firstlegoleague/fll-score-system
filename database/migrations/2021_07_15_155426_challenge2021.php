<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Challenge2021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge2021', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer("teamID");
            $table->integer("roundID");
            $table->boolean("isPublic");
            $table->integer("judge_id")->default(0);
            $table->integer("totalScore")->default(0);
            $table->integer("sendedScore")->default(0);
            $table->text("remarks")->nullable();
            $table->integer("M00_1")->default(0);
            $table->integer("M01_1")->default(0);
            $table->integer("M02_1")->default(0);
            $table->integer("M02_2")->default(0);
            $table->integer("M03_1")->default(0);
            $table->integer("M03_2")->default(0);
            $table->integer("M04_1")->default(0);
            $table->integer("M04_2")->default(0);
            $table->integer("M05_1")->default(0);
            $table->integer("M06_1")->default(0);
            $table->integer("M06_2")->default(0);
            $table->integer("M06_3")->default(0);
            $table->integer("M07_1")->default(0);
            $table->integer("M07_2")->default(0);
            $table->integer("M08_1")->default(0);
            $table->integer("M08_2")->default(0);
            $table->integer("M08_3")->default(0);
            $table->integer("M09_1")->default(0);
            $table->integer("M09_2")->default(0);
            $table->integer("M10_1")->default(0);
            $table->integer("M11_1")->default(0);
            $table->integer("M12_1")->default(0);
            $table->integer("M12_2")->default(0);
            $table->integer("M13_1")->default(0);
            $table->integer("M13_2")->default(0);
            $table->integer("M14_1")->default(0);
            $table->integer("M15_1")->default(0);
            $table->integer("M15_2")->default(0);
            $table->integer("M15_3")->default(0);
            $table->integer("M16_1")->default(0);
            $table->integer("M16_2")->default(0);
            $table->integer("M16_3")->default(0);
            $table->integer("M16_4")->default(0);
            $table->integer("M16_5")->default(0);
            $table->integer("M17_1")->default(0);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
