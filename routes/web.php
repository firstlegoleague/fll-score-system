<?php

Use App\Http\Resources\UserResource;
Use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('main');

Route::get('/home', function() {
   return view('home');
});

Route::get('/timer', function() {
    return view('timer.timer');
})->name('timer');

// Function to display the correct challenge for the settings. This way is build in
// a way that it shouldn't be changed every year.
Route::get('/challenge/{locale}/{year}', function ($locale, $year) {
    App::setLocale($locale);
    $teams = \App\Teams::all();
    $rounds = \App\Rounds::all();

    return view('challenges.'.$year, compact("teams", "rounds"));
});

Auth::routes();

Route::get('home/{locale}', 'HomeController@indexLang')->name('home')->middleware('auth');

// User Routes
Route::get('users/{locale}/list', 'UserController@list')->name("users.list")->middleware('auth');
Route::get('users/{locale}/edit/{id}', 'UserController@edit')->name("users.edit")->middleware('auth');
Route::post('users/{locale}/edit/', 'UserController@save')->name("users.save")->middleware('auth');
Route::get('users/{locale}/force/{id}', 'UserController@force')->name("users.force")->middleware('auth');
Route::post('users/{locale}/force/{id}', 'UserController@forceSet')->name("users.forcePost")->middleware('auth');

// Settings
Route::get('settings/{locale}', 'SettingsController@index')->middleware('auth');
Route::post('settings/{locale}', 'SettingsController@postSettings')->middleware('auth');

// Teams
Route::get('teams/{locale}/list', 'TeamController@list')->name('teams.list')->middleware('auth');
Route::get('teams/{locale}/add', 'TeamController@add_get')->middleware('auth');
Route::post('teams/{locale}/add', 'TeamController@add_post')->middleware('auth');
Route::get('teams/{locale}/edit/{id}', 'TeamController@edit')->name('teams.edit')->middleware('auth');
Route::post('teams/{locale}/edit/{id}', 'TeamController@save')->name('teams.save')->middleware('auth');
Route::post('teams/{locale}/delete/{id}', 'TeamController@delete')->name('teams.delete')->middleware('auth');

// Rounds
Route::get('rounds/{locale}/list', 'RoundController@list')->name('rounds.list')->middleware('auth');
Route::post('rounds/{locale}/list', 'RoundController@add')->name('rounds.add')->middleware('auth');
Route::get('rounds/{locale}/delete/{id}', 'RoundController@delete')->name('rounds.delete')->middleware('auth');
Route::get('rounds/{id}/public', 'RoundController@setPublic')->name('rounds.makePublic')->middleware('auth');
Route::get('rounds/{id}/private', 'RoundController@setPrivate')->name('rounds.makePrivate')->middleware('auth');

// Overview
Route::get('overview/{locale}/game', 'OverviewController@gameOverview')->name('overview.gametable')->middleware('auth');
Route::get('overview/{locale}/game/public', 'OverviewController@gameOverviewPublic')->name('overview.gametablepublic');


// Games
Route::get('games/{locale}/list', 'GameController@list')->name('games.list')->middleware('auth');
Route::get('games/{locale}/delete/{id}', 'GameController@delete')->name('games.delete')->middleware('auth');
Route::get('games/{locale}/edit/{id}', 'GameController@edit')->name('games.edit')->middleware('auth');
Route::post('games/edit/post', 'GameController@edit_post')->name("game.edit.post")->middleware('auth');


// Challenge 2021
Route::post('scoreform/{locale}/submit/', 'Challenge2021Controller@saveScoresheet')->name('scoreform.post.2021')->middleware('auth');
Route::post('games/edit/post/2021', 'Challenge2021Controller@editScoresheet')->name("game.edit.post.2021")->middleware('auth');

// json returns
//Route::get('users/json.json', 'UserController@asJson')->name('user.json');
// Disabled till we have a proper use for it

// Exports
Route::get('exports/gamescores', 'ExportController@gamescores')->name('export.gamescores')->middleware('auth');
Route::get('exports/remarks', 'ExportController@remarks')->name('export.remarks')->middleware('auth');

// Scoreboard
Route::get('scoreboard/{locale}', "ScoreboardController@scoreboardPublic")->name('scoreboard.public');
Route::get('scoreboard/{locale}/screen', "ScoreboardController@scoreboardScreen")->name('scoreboard.screen');
Route::get('scoreboard/{locale}/private', "ScoreboardController@scoreboardPrivate")->name('scoreboard.private')->middleware('auth');


// Account
Route::get('account/{locale}', "AccountController@view")->name('account.view')->middleware('auth');
Route::get('account/{locale}/reset', "AccountController@changePasswordPage")->name('account.changePassword');
Route::post('account/{locale}/reset','AccountController@changePassword')->name('account.changePassword.post');


// API Web
Route::get('/apiList', "apiController@get_keys_web")->middleware('auth');
Route::get('/api/generate', "apiController@create_keys_web")->middleware('auth');

// API Calls
    // API keys
Route::get('/api/{key}/keys', "apiController@get_keys");
Route::post('/api/{key}/keys', "apiController@create_keys");
Route::delete('/api/{key}/keys', "apiController@delete_keys");

    // Users
Route::get('/api/{key}/users', "apiController@get_users");
Route::post('/api/{key}/users', "apiController@post_users");
Route::delete('/api/{key}/users', "apiController@delete_users");
Route::patch('/api/{key}/users', "apiController@patch_users");

    // Games
Route::get('/api/{key}/games', "apiController@get_games");
Route::post('/api/{key}/games', "apiController@post_games");
Route::delete('/api/{key}/games', "apiController@delete_games");
Route::patch('/api/{key}/games', "apiController@patch_games");

    // Teams
Route::get('/api/{key}/teams', "apiController@get_teams");
Route::post('/api/{key}/teams', "apiController@post_teams");
Route::delete('/api/{key}/teams', "apiController@delete_teams");
Route::patch('/api/{key}/teams', "apiController@patch_teams");

    // Settings
Route::get('/api/{key}/settings', "apiController@get_settings");
Route::post('/api/{key}/settings', "apiController@post_settings");
Route::delete('/api/{key}/settings', "apiController@delete_settings");
Route::patch('/api/{key}/settings', "apiController@patch_settings");


Route::any('/api/{key}/test', "apiController@test");
Route::any('/api/{key}/teapot', "apiController@teapot");
Route::any('/api/teapot', "apiController@teapot");
