# ToDo list for this project

## API
- Create an API so things can be requested
## Wanted changes
- In the match overview show the score if its correct (so only 1 match)

## scoreboard
- Ignore practice rounds

## Overview scores
- A list with all the judge forms

## Judge Forms (Maybe, not sure yet)
- Make the judge form pages
- Implement some logic behind it
- Make database entries for the judge forms
- Something that the team can later see the forms.

## Useradmin
 - Create users form this dashboard
 - Send email on user self registering to organizer
 - Send email to user on create user by organizer

## Something else?
- So far, no
