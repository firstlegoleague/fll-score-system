FROM existenz/webstack:7.4

COPY --chown=php:nginx ./ /www
COPY --chown=php:nginx ./.env.example /www/.env

RUN find /www -type d -exec chmod -R 555 {} \; \
    && find /www -type f -exec chmod -R 444 {} \; \
    && apk -U --no-cache add \
    git \
    nodejs \
    npm \
    php7 \
    php7-ctype \
    php7-dom \
    php7-fileinfo \
    php7-gd \
    php7-iconv \
    php7-json \
    php7-mbstring \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-phar \
    php7-session \
    php7-simplexml \
    php7-tokenizer \
    bind-tools \
    php7-xml \
    php7-xmlreader \
    php7-xmlwriter \
    php7-zip

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /www

RUN chmod 777 /www/storage/ -R

RUN ["/usr/bin/composer", "config", "--global", "disable-tls", "true"]
RUN ["/usr/bin/composer", "install"]

# Frontend
RUN npm install
RUN npm run production
