<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;


class Challenge2021Controller extends Controller
{

    public function saveScoresheet(Request $request, $locale)
    {
        App::setLocale($locale);

        $info = $request->all();

        $newScore = new \App\challenge2021();

        $newScore->M00_1 = $info["M00_1"];
        $newScore->M01_1 = $info["M01_1"];
        $newScore->M02_1 = $info["M02_1"];
        $newScore->M02_2 = $info["M02_2"];
        $newScore->M03_1 = $info["M03_1"];
        $newScore->M03_2 = $info["M03_2"];
        $newScore->M04_1 = $info["M04_1"];
        $newScore->M04_2 = $info["M04_2"];
        $newScore->M05_1 = $info["M05_1"];
        $newScore->M06_1 = $info["M06_1"];
        $newScore->M06_2 = $info["M06_2"];
        $newScore->M06_3 = $info["M06_3"];
        $newScore->M07_1 = $info["M07_1"];
        $newScore->M07_2 = $info["M07_2"];
        $newScore->M08_1 = $info["M08_1"];
        $newScore->M08_2 = $info["M08_2"];
        $newScore->M08_3 = $info["M08_3"];
        $newScore->M09_1 = $info["M09_1"];
        $newScore->M09_2 = $info["M09_2"];
        $newScore->M10_1 = $info["M10_1"];
        $newScore->M11_1 = $info["M11_1"];
        $newScore->M12_1 = $info["M12_1"];
        $newScore->M12_2 = $info["M12_2"];
        $newScore->M13_1 = $info["M13_1"];
        $newScore->M13_2 = $info["M13_2"];
        $newScore->M14_1 = $info["M14_1"];
        $newScore->M15_1 = $info["M15_1"];
        $newScore->M15_2 = $info["M15_2"];
        $newScore->M15_3 = $info["M15_3"];
        $newScore->M16_1 = $info["M16_1"];
        $newScore->M16_2 = $info["M16_2"];
        $newScore->M16_3 = $info["M16_3"];
        $newScore->M16_4 = $info["M16_4"];
        $newScore->M16_5 = $info["M16_5"];
        $newScore->M17_1 = $info["M17_1"];
        $newScore->GP_1 = $info["GP_1"];

//      Get info, and create relational data from it
        $teamfull = $info["team"];
        $teamnumber = explode(" ", $teamfull)[0];
        $team = \App\Teams::all()->where("teamNumber", $teamnumber)->first();
        $round = \App\Rounds::all()->where("round", $info["round"])->first();

//      Form the other info. Create everything you need.
        $newScore->teamID = $team->id;
        $newScore->roundID = $round->id;
        $newScore->judge_id = Auth::user()->id;
        $newScore->sendedScore = $info["totalScore"];
        $newScore->remarks = $info["remarks"];
        $newScore->isPublic =  \App\Rounds::all()->where('id', $round->id)->first()->public; // \App\settings::getGamePublic();

        // And now the fun part. Calculating the score!

        $score = 0;

        // M00
        $scoreM = 0;
        if ($newScore->M00_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        //M01
        $scoreM = 0;
        if ($newScore->M01_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        //M02
        $scoreM = 0;

        if($newScore->M02_2 == 0){
            $scoreM = 0;
        } else if($newScore->M02_2 == 1){
            $scoreM = 20;
        } else if($newScore->M02_2 == 2){
            $scoreM = 30;
        }

        if($newScore->M02_1 == 0){
            $scoreM = 0;
        }

        $score = $score + $scoreM;

        //M03
        $scoreM = 0;
        if ($newScore->M03_1 == 1) {
            $scoreM = $scoreM + 20;
        }

        if ($newScore->M03_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        $score = $score + $scoreM;

        //M04
        $scoreM = 0;
        if ($newScore->M04_1 == 1 && $newScore->M04_2 == 1) {
            $scoreM = 30;
        } else if ($newScore->M04_1 == 1 || $newScore->M04_2 == 1) {
            $scoreM = 10;
        }
        $score = $score + $scoreM;



        //M05
        $scoreM = 0;
        if ($newScore->M05_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        //M06
        $scoreM = 0;
        if ($newScore->M06_1 == 1) {
            $scoreM = 20;
        }
        if ($newScore->M06_1 == 1 && $newScore->M06_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        if ($newScore->M06_3 == 1) {
            $scoreM = 0;
        }
        $score = $score + $scoreM;

        //M07
        $scoreM = 0;
        if ($newScore->M07_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M07_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        $score = $score + $scoreM;

        //M08
        $scoreM = 0;
        if ($newScore->M08_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M08_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        if ($newScore->M08_3 == 1) {
            $scoreM = $scoreM + 10;
        }
        $score = $score + $scoreM;

        //M09
        $scoreM = 0;
        if ($newScore->M09_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M09_2 == 1) {
            $scoreM = $scoreM + 20;
        }
        $score = $score + $scoreM;

        //M10
        $scoreM = 0;
        if ($newScore->M10_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        $score = $score + $scoreM;

        //M11
        $scoreM = 0;
        if ($newScore->M11_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M11_1 == 2) {
            $scoreM = $scoreM + 30;
        }
        $score = $score + $scoreM;

        //M12
        $scoreM = 0;
        if ($newScore->M12_1 == 1) {
            $scoreM = $scoreM + 20;
        } else if ($newScore->M12_1 == 2) {
        $scoreM = $scoreM + 30;
        }

        if ($newScore->M12_2 == 1) {
            $scoreM = $scoreM + 5;
        } else if ($newScore->M12_2 == 2) {
            $scoreM = $scoreM + 10;
        }

        $score = $score + $scoreM;

        //M13
        $scoreM = 0;
        if($newScore->M13_1 == 1 && $newScore->M13_2 == 1){
            $scoreM = $scoreM + 30;
        } else if($newScore->M13_1 == 1 || $newScore->M13_2 == 1){
            $scoreM = $scoreM + 10;
        }

        $score = $score + $scoreM;

        //M14
        $scoreM = 0;
        if ($newScore->M14_1 == 1) {
            $scoreM = $scoreM + 10;
        } else if ($newScore->M14_1 == 2) {
            $scoreM = $scoreM + 20;
        }
        $score = $score + $scoreM;

        //M15
        $scoreM = 0;
        if ($newScore->M15_1 == 1) {
            $scoreM = $scoreM + 10;
        } else if ($newScore->M15_1 == 2) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M15_2 == 1) {
            $scoreM = $scoreM + 20;
        } else if ($newScore->M15_2 == 2) {
            $scoreM = $scoreM + 40;
        }
        if ($newScore->M15_3 == 1) {
            $scoreM = $scoreM + 30;
        } else if ($newScore->M15_3 == 2) {
            $scoreM = $scoreM + 60;
        }
        $score = $score + $scoreM;

        //M16
        $scoreM = 0;

        // Stupid, but never trust user input
        if(($newScore->M16_1 + $newScore->M16_2) > 8){
            // They cheat, so they won't get any points for this mission
            $newScore->M16_1 = 8;
            $newScore->M16_2 = 0;
            $newScore->M16_3 = 0;
            $newScore->M16_4 = 0;
            $newScore->M16_5 = 0;
        }

        $scoreM = $scoreM + ($newScore->M16_1 * 5);
        $scoreM = $scoreM + ($newScore->M16_2 * 10);
        if($newScore->M16_3 == 1) {
            $scoreM = $scoreM + 20;
        }
        if($newScore->M16_4 == 1) {
            $scoreM = $scoreM + 20;
        }
        $scoreM = $scoreM + ($newScore->M16_5 * 10);

        $score = $score + $scoreM;

        //M17
        $scoreM = 0;
        if($newScore->M17_1 == 1){
            $scoreM = 10;
        } else if($newScore->M17_1 == 2){
            $scoreM = 15;
        } else if($newScore->M17_1 == 3) {
            $scoreM = 25;
        } else if($newScore->M17_1 == 4){
            $scoreM = 35;
        } else if($newScore->M17_1 == 5){
            $scoreM = 50;
        } else if($newScore->M17_1 == 6){
            $scoreM = 50;
        }
        $score = $score + $scoreM;

        $newScore->totalScore = $score;
        $newScore->save();

        return Redirect()->back()->with("successSaved", "true");
    }

    public function editScoresheet(Request $request)
    {
        $info = $request->all();

        if(isset($info["game_id"]))
            $newScore = \App\challenge2021::all()->where("id", $info["game_id"])->first();
        elseif (isset($info["id"]))
            $newScore = \App\challenge2021::all()->where("id", $info["id"])->first();

        if(isset($info["round"]))
            $round = \App\Rounds::all()->where("round", $info["round"])->first();
        elseif(isset($info["roundID"]))
            $round = \App\Rounds::all()->where("id", $info["roundID"])->first();

        $newScore->roundID = $round->id;

        $newScore->M00_1 = $info["M00_1"];
        $newScore->M01_1 = $info["M01_1"];
        $newScore->M02_1 = $info["M02_1"];
        $newScore->M02_2 = $info["M02_2"];
        $newScore->M03_1 = $info["M03_1"];
        $newScore->M03_2 = $info["M03_2"];
        $newScore->M04_1 = $info["M04_1"];
        $newScore->M04_2 = $info["M04_2"];
        $newScore->M05_1 = $info["M05_1"];
        $newScore->M06_1 = $info["M06_1"];
        $newScore->M06_2 = $info["M06_2"];
        $newScore->M06_3 = $info["M06_3"];
        $newScore->M07_1 = $info["M07_1"];
        $newScore->M07_2 = $info["M07_2"];
        $newScore->M08_1 = $info["M08_1"];
        $newScore->M08_2 = $info["M08_2"];
        $newScore->M08_3 = $info["M08_3"];
        $newScore->M09_1 = $info["M09_1"];
        $newScore->M09_2 = $info["M09_2"];
        $newScore->M10_1 = $info["M10_1"];
        $newScore->M11_1 = $info["M11_1"];
        $newScore->M12_1 = $info["M12_1"];
        $newScore->M12_2 = $info["M12_2"];
        $newScore->M13_1 = $info["M13_1"];
        $newScore->M13_2 = $info["M13_2"];
        $newScore->M14_1 = $info["M14_1"];
        $newScore->M15_1 = $info["M15_1"];
        $newScore->M15_2 = $info["M15_2"];
        $newScore->M15_3 = $info["M15_3"];
        $newScore->M16_1 = $info["M16_1"];
        $newScore->M16_2 = $info["M16_2"];
        $newScore->M16_3 = $info["M16_3"];
        $newScore->M16_4 = $info["M16_4"];
        $newScore->M16_5 = $info["M16_5"];
        $newScore->M17_1 = $info["M17_1"];
        $newScore->GP_1 = $info["GP_1"];

        $newScore->remarks = $info["remarks"];
        $newScore->isPublic = \App\settings::getGamePublic();

        // And now the fun part. Calculating the score!

        $score = 0;

        // M00
        $scoreM = 0;
        if ($newScore->M00_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        $score = 0;

        // M00
        $scoreM = 0;
        if ($newScore->M00_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        //M01
        $scoreM = 0;
        if ($newScore->M01_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        //M02
        $scoreM = 0;

        if($newScore->M02_2 == 0){
            $scoreM = 0;
        } else if($newScore->M02_2 == 1){
            $scoreM = 20;
        } else if($newScore->M02_2 == 2){
            $scoreM = 30;
        }

        if($newScore->M02_1 == 0){
            $scoreM = 0;
        }

        $score = $score + $scoreM;

        //M03
        $scoreM = 0;
        if ($newScore->M03_1 == 1) {
            $scoreM = $scoreM + 20;
        }

        if ($newScore->M03_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        $score = $score + $scoreM;

        //M04
        $scoreM = 0;
        if ($newScore->M04_1 == 1 && $newScore->M04_2 == 1) {
            $scoreM = 30;
        } else if ($newScore->M04_1 == 1 || $newScore->M04_2 == 1) {
            $scoreM = 10;
        }
        $score = $score + $scoreM;



        //M05
        $scoreM = 0;
        if ($newScore->M05_1 == 1) {
            $scoreM = 20;
        }
        $score = $score + $scoreM;

        //M06
        $scoreM = 0;
        if ($newScore->M06_1 == 1) {
            $scoreM = 20;
        }
        if ($newScore->M06_1 == 1 && $newScore->M06_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        if ($newScore->M06_3 == 1) {
            $scoreM = 0;
        }
        $score = $score + $scoreM;

        //M07
        $scoreM = 0;
        if ($newScore->M07_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M07_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        $score = $score + $scoreM;

        //M08
        $scoreM = 0;
        if ($newScore->M08_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M08_2 == 1) {
            $scoreM = $scoreM + 10;
        }
        if ($newScore->M08_3 == 1) {
            $scoreM = $scoreM + 10;
        }
        $score = $score + $scoreM;

        //M09
        $scoreM = 0;
        if ($newScore->M09_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M09_2 == 1) {
            $scoreM = $scoreM + 20;
        }
        $score = $score + $scoreM;

        //M10
        $scoreM = 0;
        if ($newScore->M10_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        $score = $score + $scoreM;

        //M11
        $scoreM = 0;
        if ($newScore->M11_1 == 1) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M11_1 == 2) {
            $scoreM = $scoreM + 30;
        }
        $score = $score + $scoreM;

        //M12
        $scoreM = 0;
        if ($newScore->M12_1 == 1) {
            $scoreM = $scoreM + 20;
        } else if ($newScore->M12_1 == 2) {
            $scoreM = $scoreM + 30;
        }

        if ($newScore->M12_2 == 1) {
            $scoreM = $scoreM + 5;
        } else if ($newScore->M12_2 == 2) {
            $scoreM = $scoreM + 10;
        }

        $score = $score + $scoreM;

        //M13
        $scoreM = 0;
        if($newScore->M13_1 == 1 && $newScore->M13_2 == 1){
            $scoreM = $scoreM + 30;
        } else if($newScore->M13_1 == 1 || $newScore->M13_2 == 1){
            $scoreM = $scoreM + 10;
        }

        $score = $score + $scoreM;

        //M14
        $scoreM = 0;
        if ($newScore->M14_1 == 1) {
            $scoreM = $scoreM + 10;
        } else if ($newScore->M14_1 == 2) {
            $scoreM = $scoreM + 20;
        }
        $score = $score + $scoreM;

        //M15
        $scoreM = 0;
        if ($newScore->M15_1 == 1) {
            $scoreM = $scoreM + 10;
        } else if ($newScore->M15_1 == 2) {
            $scoreM = $scoreM + 20;
        }
        if ($newScore->M15_2 == 1) {
            $scoreM = $scoreM + 20;
        } else if ($newScore->M15_2 == 2) {
            $scoreM = $scoreM + 40;
        }
        if ($newScore->M15_3 == 1) {
            $scoreM = $scoreM + 30;
        } else if ($newScore->M15_3 == 2) {
            $scoreM = $scoreM + 60;
        }
        $score = $score + $scoreM;

        //M16
        $scoreM = 0;

        // Stupid, but never trust user input
        if(($newScore->M16_1 + $newScore->M16_2) > 8){
            // They cheat, so they won't get any points for this mission
            $newScore->M16_1 = 8;
            $newScore->M16_2 = 0;
            $newScore->M16_3 = 0;
            $newScore->M16_4 = 0;
            $newScore->M16_5 = 0;
        }

        $scoreM = $scoreM + ($newScore->M16_1 * 5);
        $scoreM = $scoreM + ($newScore->M16_2 * 10);
        if($newScore->M16_3 == 1) {
            $scoreM = $scoreM + 20;
        }
        if($newScore->M16_4 == 1) {
            $scoreM = $scoreM + 20;
        }
        $scoreM = $scoreM + ($newScore->M16_5 * 10);

        $score = $score + $scoreM;

        //M17
        $scoreM = 0;
        if($newScore->M17_1 == 1){
            $scoreM = 10;
        } else if($newScore->M17_1 == 2){
            $scoreM = 15;
        } else if($newScore->M17_1 == 3) {
            $scoreM = 25;
        } else if($newScore->M17_1 == 4){
            $scoreM = 35;
        } else if($newScore->M17_1 == 5){
            $scoreM = 50;
        } else if($newScore->M17_1 == 6){
            $scoreM = 50;
        }
        $score = $score + $scoreM;

        $newScore->save();

        return Redirect::back();
    }


}
