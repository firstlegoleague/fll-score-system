<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RoundController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list($locale){
        App::setLocale($locale);
        $rounds = \App\Rounds::all();

        return view("rounds.list", compact("rounds"));
    }

    public function add(Request $request, $locale){
        App::setLocale($locale);

        $newRound = new \App\Rounds();
        $info = $request->request->all();

        $newRound->round = $info["roundname"];
        $newRound->save();


        $rounds = \App\Rounds::all();
        return view("rounds.list", compact("rounds"));
    }

    public function delete($locale, $id){
        $round = \App\Rounds::all()->where('id', $id)->first();
        $round->delete();

        return redirect()->route('rounds.list', ['locale'=>$locale]);
    }

    public function setPublic($id){
        $round = \App\Rounds::all()->where('id', $id)->first();
        $round->public = 1;
        $round->save();

        if(\App\settings::getSeason() == "2020"){
            \App\challenge2020::where('roundID', $id)->update(['isPublic'=>1]);
        } else if(\App\settings::getSeason() == "2021"){
            \App\challenge2021::where('roundID', $id)->update(['isPublic'=>1]);
        }
    }


    public function setPrivate($id){
        $round = \App\Rounds::all()->where('id', $id)->first();
        $round->public = 0;
        $round->save();

        if(\App\settings::getSeason() == "2020"){
            \App\challenge2020::where('roundID', $id)->update(['isPublic'=>0]);
        } else if(\App\settings::getSeason() == "2021"){
            \App\challenge2021::where('roundID', $id)->update(['isPublic'=>0]);
        }

    }
}
