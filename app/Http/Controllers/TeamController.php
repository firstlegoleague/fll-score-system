<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list($locale){
        App::setLocale($locale);
        $teams = \App\Teams::all();

        return view("teams.list", compact("teams"));
    }

    public function add_get($locale){
        App::setLocale($locale);
        return view('teams.add');
    }

    public function add_post(Request $request, $locale){
        App::setLocale($locale);
        $info = $request->request->all();
        $team = new \App\Teams();

        $team->teamNumber = $info["teamnumber"];
        $team->teamname = $info["teamname"];
        $team->affiliate = $info["affiliate"];
        $team->save();


        return redirect()->route('teams.list', ['locale'=>$locale]);
    }

    public function edit($locale, $id){
        App::setLocale($locale);

        $team = \App\Teams::all()->where('id', $id)->first();

        return view('teams.edit', compact("team"));
    }

    public function save($locale, Request $request){
        App::setLocale($locale);

        $info = $request->all();
        $team = \App\Teams::all()->where('teamNumber', $info['teamnumber'])->first();

        $team->teamname = $info['teamname'];
        $team->affiliate = $info['affiliate'];
        $team->update();

        return redirect()->route('teams.list', ['locale'=>$locale])->with("teamsaved", 'true');
    }

    public function delete($locale, $id,Request $request){
        App::setLocale($locale);

        $info = $request->all();
        $team = \App\Teams::all()->where('id', $id)->first();

        $team->delete();

        return redirect()->route('teams.list', ['locale'=>$locale])->with("teamdeleted", 'true');
    }
}
