<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Arr;

class SettingsController extends Controller
{
    public function index($locale){
        App::setLocale($locale);
        return view('settings.settings');
    }

    public function postSettings(Request $request, $locale){
        App::setLocale($locale);

        $info = $request->request->all();

        $finalName = \App\settings::all()->where('key', 'finalName')->first();
        $season = \App\settings::all()->where('key', 'season')->first();
        $register = \App\settings::all()->where('key', 'register')->first();
        $public = \App\settings::all()->where('key', 'gamePublic')->first();

        $finalName->value = $info["finalname"];
        $season->value = $info["season"];

        if(Arr::exists($info, 'registerEnabled')){
            $register->value = "1";
        } else {
            $register->value = "0";
        }

        if(Arr::exists($info, 'scorePublic')){
            $public->value = "1";
        } else {
            $public->value = "0";
        }

        $finalName->save();
        $season->save();
        $register->save();
        $public->save();

        return view('settings.settings');


    }
}
