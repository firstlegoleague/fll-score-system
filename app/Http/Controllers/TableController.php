<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TableController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list($locale){
        App::setLocale($locale);
        $tables = \App\Tables::all();

        return view("rounds.list", compact("tables"));
    }

    public function add(Request $request, $locale){
        App::setLocale($locale);

        $newTable = new \App\Rounds();
        $info = $request->request->all();

        $newTable->round = $info["tablename"];
        $newTable->save();


        $tables = \App\Rounds::all();
        return view("tables.list", compact("tables"));
    }

    public function delete($locale, $id){
        $table = \App\Tables::all()->where('id', $id)->first();
        $table->delete();

        return redirect()->route('tables.list', ['locale'=>$locale]);
    }
}
