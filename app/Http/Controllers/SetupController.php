<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SetupController extends Controller
{
    //

    public function setupSettings(){

         $finalName = new \App\settings();
         $season = new \App\settings();
         $register = new \App\settings();
         $gamePublic = new \App\settings();

         $finalName->key = "finalName";
         $season->key = "season";
         $register->key = "register";
         $gamePublic->key = "gamePublic";

         $finalName->value = "NotSet";
         $season->value = "2021";
         $register->value = "1";
         $gamePublic->value = "0";

         $finalName->save();
         $season->save();
         $register->save();
         $gamePublic->save();

        return;
    }
}
