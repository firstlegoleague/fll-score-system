<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function asJson(){
        $users = \App\User::all();
        return $users->toJson();
    }

    public function list($locale){
        // Set the requested locale
        App::setLocale($locale);

        // Check if the user can do this
        $user = Auth::user();
        if($user->can('users.view') != true){
            abort(401);
        }

        $users = \App\User::all();
        return view('user.list', compact("users") );
    }

    public function edit($locale, $id){
        // Set the requested locale
        App::setLocale($locale);

        // Check if the user can do this
        $user = Auth::user();
        if($user->can('users.edit') != true){
            abort(401);
        }

        $user = \App\User::all()->where('id', $id)->first();

        # Roles to check what roles there are in the dropdown menu
        $roles = \App\roles::all();

        return view('user.edit', compact("user", "roles") );
    }

    public function save($locale, Request $request){

        $info = $request->all();

        $user = \App\User::all()->where('id', $info['id'])->first();

//        dd($info);

        $user->email = $info['email'];
        $user->name = $info['name'];

        $user->removeRole($user->roles->first()->id);
        $user->assignRole($info['role']);

        $user->update();

        return redirect()->route('users.list', ['locale'=>$locale]);
    }

    public function force($locale, $id){
        App::setLocale($locale);
        $user = \App\User::all()->where('id', $id)->first();
        return view('user.forcePassword', ['locale'=>$locale, 'user'=>$user]);
    }

    public function forceSet(Request $request, $locale, $id){
        $info = $request->all();
        $changeUser = \App\User::all()->where('id', $id)->first();
        $user = Auth::user();
        $user->can('users.edit');

        $changeUser->password = Hash::make($info['newPassword']);
        $changeUser->save();

        return redirect()->route('users.list', ['locale'=>$locale]);
    }
}
