<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;


class apiController extends Controller
{

    public function checkKey($key){
        $validKey = \App\api::all()->where('apikey', $key)->first();
        if($validKey == NULL or $validKey->enabled == 0)
            return response('{"code":401,"response":"Not allowed"}', 401);
        ;

        return $validKey;
    }

    // To check if your API key is valid
    public function test($key){
        $validkey = $this->checkKey($key);
        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }

    // I'm a teapot!
    public function teapot(){
        return response('{"code":418,"response":"I\'m a teapot!"}', 418)->header('Content-Type', 'text/json');
    }

    public function get_keys_web(){
        $apikeys = \App\api::all();
        return view('api.list',compact("apikeys"));
    }

    public function create_keys_web(){
        // Create pool of characters where we create a apikey from
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // Create the key itself, suffix of FLL_
        $key = "FLL_".substr(str_shuffle(str_repeat($pool, 5)), 0, 60);
        // Get the user that is creating an API key
        $user = Auth::user()->id;
        // Create the API key
        $apikey = new \App\api;
        // Set all the variables
        $apikey['apikey'] = $key;
        $apikey['userID'] = $user;
        $apikey['enabled'] = 1;
        // Save the complete key
        $apikey->save();
        // Return the API key to the user
        return $key;
    }

    /**
     * @brief API interface to get the keys
     * @param $key API key
     * @return In case of good API key,
     */
    public function get_keys($key){
        $this->checkKey($key);
        return \App\api::all()->toJson();
    }
    public function create_keys($key){
        $validKey = $this->checkKey($key);

        // Create pool of characters where we create a apikey from
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // Create the key itself, suffix of FLL_
        $key = "FLL_".substr(str_shuffle(str_repeat($pool, 5)), 0, 60);
        // Get the user that is creating an API key
        $user = $validKey->userID;
        // Create the API key
        $apikey = new \App\api;
        // Set all the variables
        $apikey['apikey'] = $key;
        $apikey['userID'] = $user;
        $apikey['enabled'] = 1;
        // Save the complete key
        $apikey->save();
        // Return the API key to the user
        return "{\"apikey:\"".$key."\"}";
    }

    public function delete_keys($key, Request $request){
        $validkey = $this->checkKey($key);
        $request = $request->all();

        $disableKey = \App\api::all()->where('id', $request['id'])->first();

        if($validkey->id == $disableKey->id)
            return response('{"code":403,"response":"Not allowed to remove yourself"}', 406)->header('Content-Type', 'text/json');

        if($validkey->userID != $disableKey->userID)
            return response('{"code":401,"response":"Not allowed to API keys from others"}', 401)->header('Content-Type', 'text/json');

        if($disableKey->enabled == 0)
            return response('{"code":400,"response":"Key is already disabled"}', 400)->header('Content-Type', 'text/json');

        $disableKey->enabled = 0;
        $disableKey->save();

        return response('{"code":200,"response":"success"}', 200)->header('Content-Type', 'text/json');
    }

    public function get_users($key){
        $validkey = $this->checkKey($key);
        return \App\User::all()->toJson();
    }
    public function post_users($key, Request $request){
        $validkey = $this->checkKey($key);

        // Check if all variables are there
        if($request->exists('email') == false ||
            $request->exists('name') == false ||
            $request->exists('password') == false){
            return response('{"code":417,"response":"Not all parameters has been supplied"}', 417);
        }

        // Check if the password is at least 6 chars long
        if(strlen($request->all()['password']) <= 6){
            return response('{"code":411,"response":"Password is not long enough"}', 411);
        }

        // Check if the user already exsist
        $checkUser = \App\User::all()->where("email", $request->all()["email"])->first();
        if($checkUser != NULL){
            return response('{"code":409,"response":"Email is already registered"}', 409);
        }

        // Create the user
        $user = new \App\User();
        $user->name = $request->all()['name'];
        $user->email = $request->all()['email'];
        $user->password = Hash::make($request->all()['password']);
        $user->save();

        // If a role has been supplied, also apply the role
        if($request->exists("role")){
            // Not allowed to create a super-admin
            if($request->all()["role"] == "super-admin"){
                // Remove the user
                $user->delete();
                // Send respond
                return response('{"code":409,"response":"Role not allowed"}', 409);
            }
            // Assign the role
            $role = Role::findByName($request->all()["role"]);
            $role->users()->attach($user);
        }

        // Return that it has been a success
        return response('{"code":200,"response":"Success","info":"User id:'.$user->id.'"}', 200)->header('Content-Type', 'text/json');
    }
    public function delete_users($key, Request $request){
        $validkey = $this->checkKey($key);

        if($validkey->userID == $request->all()['id']){
            return response('{"code":409,"response":"Not allowed to remove yourself"}', 409)->header('Content-Type', 'text/json');
        }

        $user = \App\User::all()->where('id', $request->all()['id'])->first();

        if($user->hasRole('super-admin'))
            return response('{"code":409,"response":"Not allowed to remove super-admin"}', 409)->header('Content-Type', 'text/json');

        $user->delete();
        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
    public function patch_users($key, Request $request){
        $validkey = $this->checkKey($key);

        $user = \App\User::all()->where('id', $request->all()['id'])->first();

        if($user->hasRole('super-admin') && $validkey->userID != $request->all()['id'])
            return response('{"code":401,"response":"Not allowed to change admin user"}', 401)->header('Content-Type', 'text/json');

        if($request->exists("name"))
            $user->name = $request->all()['name'];

        if($request->exists("email"))
            $user->email = $request->all()['email'];

        if($request->exists("email"))
            $user->password = Hash::make($request->all()['password']);

        $user->save();
        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }

    public function get_games($key, Request $request){
        $validkey = $this->checkKey($key);
        $year = \App\settings::getSeason();

        if($request->exists("id")){
            if($year == 2020){
                $games = \App\challenge2020::all()->where('id', $request->all()['id'])->first();
            }
            else if($year == 2021){
                $games = \App\challenge2021::all()->where('id', $request->all()['id'])->first();
            }
            else {
                return false;
            }
        } else {
            if($year == 2020){
                $games = \App\challenge2020::all();
            }
            else if($year == 2021){
                $games = \App\challenge2021::all();
            }
            else {
                return false;
            }
        }

        return $games->toJson();
    }
    public function post_games($key, Request $request){
        $validkey = $this->checkKey($key);

        $year = \App\settings::getSeason();
        if($year == 2020){
            app('App\Http\Controllers\Challenge2020Controller')->saveScoresheet($request);
        }
        else if($year == 2021){
            app('App\Http\Controllers\Challenge2021Controller')->saveScoresheet($request);
        }
        else {
            return false;
        }

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
    public function delete_games($key, Request $request){
       $validkey = $this->checkKey($key);
        $year = \App\settings::getSeason();
        if($year == 2020){
            $game = \App\challenge2020::all()->where('id', $request->all()['id'])->first();
        }
        else if($year == 2021){
            $game = \App\challenge2021::all()->where('id', $request->all()['id'])->first();
        }
        else {
            return false;
        }

        $game->delete();

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
    public function patch_games($key, Request $request){
        $validkey = $this->checkKey($key);
        $year = \App\settings::getSeason();
        if($year == 2020){
            app('App\Http\Controllers\Challenge2020Controller')->editScoresheet($request);
        }
        else if($year == 2021){
            app('App\Http\Controllers\Challenge2021Controller')->editScoresheet($request);
        }
        else {
            return false;
        }
        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }

    public function get_teams($key){
        $validkey = $this->checkKey($key);
        // Get all the teams
        $teams = \App\Teams::all();

        // Return as json
        return $teams->toJson();
    }
    public function post_teams($key, Request $request){
        $validkey = $this->checkKey($key);

        if($request->exists("teamname") == false ||
            $request->exists("teamnumber") == false)
        {
            return response('{"code":417,"response":"Not all parameters has been supplied"}', 417)->header('Content-Type', 'text/json');
        }

        $existingTeam  = \App\Teams::all()->where("teamNumber", $request->all()['teamnumber'])->first();
        if($existingTeam != NULL){
            return response('{"code":409,"response":"Teamnumber already exsists"}', 409)->header('Content-Type', 'text/json');
        }

        $team = new \App\Teams;
        $team->teamname = $request->all()['teamname'];
        $team->teamNumber = $request->all()['teamnumber'];

        if($request->exists("affiliate"))
            $team->affiliate = $request->all()['affiliate'];

        $team->save();

        return response('{"code":200,"response":"Success","info":"New teamid: '.$team->id.'"}', 200)->header('Content-Type', 'text/json');
    }
    public function delete_teams($key, Request $request){
        $validkey = $this->checkKey($key);

        // Get the team
        $team = \App\Teams::all()->where('id', $request->all()['id'])->first();

        // Check if the team has results
        if(\App\Teams::hasResults($request->all()['id']))
            return response('{"code":409,"response":"Not allowed to remove team who already has played games"}', 409)->header('Content-Type', 'text/json');

        // Remove
        $team->delete();
        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
    public function patch_teams($key, Request $request){
        $validkey = $this->checkKey($key);
        $team = \App\Teams::all()->where('id', $request->all()['id'])->first();

        if($request->exists('teamnumber'))
            $team['teamNumber'] = $request->all()['teamnumber'];

        if($request->exists('teamname'))
            $team['teamname'] = $request->all()['teamname'];

        if($request->exists('affiliate'))
            $team['affiliate'] = $request->all()['affiliate'];

        $team->save();

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }

    public function get_settings($key, Request $request){
        $validkey = $this->checkKey($key);
        if($request->exists("key")){
            return \App\settings::all()->where("key", $request->all()['key'])->toJson();
        }
        return \App\settings::all()->toJson();
    }
    public function post_settings($key, Request $request){
        $validkey = $this->checkKey($key);
        if($validkey->userID =! 1){
            return response('{"code":401,"response":"Request not allowed"}', 401)->header('Content-Type', 'text/json');
        }

        $set = new \App\settings;
        $set->key = $request->all()["key"];
        $set->value = $request->all()["value"];
        $set->save();

        return response('{"code":200,"response":"Success"}', 200);}
    public function delete_settings($key, Request $request){return response('{"code":401,"response":"Request not allowed"}', 401)->header('Content-Type', 'text/json');}
    public function patch_settings($key, Request $request){
        $validkey = $this->checkKey($key);
        if(!$request->exists(["key", "value"])){
            return response('{"code":417,"response":"Not all parameters are supplied"}', 417)->header('Content-Type', 'text/json');
        }

        $setting = \App\settings::all()->where("key", $request->all()["key"])->first();
        $setting->value = $request->all()["value"];
        $setting->save();

        return response('"{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }



}
