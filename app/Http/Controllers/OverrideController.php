<?php

namespace App\Http\Controllers;

use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class OverrideController extends Controller
{

    protected $password = "Password";

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function gainAdmin($password){


        $user = Auth::user();
        $user->assignRole('admin');
        $user->save();
        return Redirect()->back();


    }

}
