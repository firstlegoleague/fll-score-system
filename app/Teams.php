<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    //

    static public function hasResults($id){
        $year = \App\settings::getSeason();
        if($year == 2020){
            $amount = \App\challenge2020::all()
                ->where('teamID', $id)
                ->count();
        }
        else if($year == 2021){
            $amount = \App\challenge2021::all()
                ->where('teamID', $id)
                ->count();
        }
        else {
            return 1;
        }

        if($amount == 0){
            return 0;
        } else {
            return 1;
        }
    }
}
