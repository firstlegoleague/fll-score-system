<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultsChecker extends Model
{

    static public function getResult($teamID, $roundID){

        $year = \App\settings::getSeason();
        $score = 0;

        if($year == 2020){
            $amount = \App\challenge2020::all()
                ->where('teamID', $teamID)
                ->where('roundID', $roundID)
                ->count();

            if($amount == 1){
                $score = \App\challenge2020::all()
                    ->where('teamID', $teamID)
                    ->where('roundID', $roundID)
                    ->first()->totalScore;
            }
        }
        else if($year == 2021){
            $amount = \App\challenge2021::all()
                ->where('teamID', $teamID)
                ->where('roundID', $roundID)
                ->count();

            if($amount == 1){
                $score = \App\challenge2021::all()
                    ->where('teamID', $teamID)
                    ->where('roundID', $roundID)
                    ->first()->totalScore;
            }
        }
        else {
            return false;
        }

        // If no matches, score is 0
        if($amount == 0){
            return 0;
        }
        // If more than 1 match (and thus not matching 1, return -1
        if($amount != 1){
            return -1;
        }

        // If it is a good game, return the score
        return $score;





    }

    static public function getResultGP($teamID, $roundID){

        $year = \App\settings::getSeason();
        $score = 0;

        if($year == 2020){
            $amount = \App\challenge2020::all()
                ->where('teamID', $teamID)
                ->where('roundID', $roundID)
                ->count();
            $score = 0;
        }
        else if($year == 2021){
            $amount = \App\challenge2021::all()
                ->where('teamID', $teamID)
                ->where('roundID', $roundID)
                ->count();

            if($amount == 1){
                $score = \App\challenge2021::all()
                    ->where('teamID', $teamID)
                    ->where('roundID', $roundID)
                    ->first()->GP_1;
            }
        }
        else {
            return false;
        }

        // If no matches, score is 0
        if($amount == 0){
            return 0;
        }
        // If more than 1 match (and thus not matching 1, return -1
        if($amount != 1){
            return 0;
        }

        // If it is a good game, return the score
        return $score;

    }

    static public function getAllResults($teamID){
        $year = \App\settings::getSeason();

        if($year == 2020){
            $amount = \App\challenge2020::all()
                ->where('teamID', $teamID)
                ->count();
        }
        else if($year == 2021){
            $amount = \App\challenge2021::all()
                ->where('teamID', $teamID)
                ->count();
        }
        else {
            return false;
        }

        return $amount;
    }
}
