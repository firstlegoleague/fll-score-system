<?php

namespace App\Exports;

use App\challenge2021;
use App\Teams;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Round;

class ChallengeExport2021 implements FromView
{
    public function view(): View
    {
        $rounds = \App\Rounds::all();
        $teams = \App\Teams::all()->sortBy('teamNumber');
        return view('exports.gamescores', compact("rounds","teams"));
    }
}
