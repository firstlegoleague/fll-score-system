<?php

namespace App\Exports;
use App\Teams;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RemarksExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        if (\App\settings::getSeason() == 2020) {
            $remarks = \App\challenge2020::all()->whereNotNull('remarks');
        }

        if (\App\settings::getSeason() == 2021) {
            $remarks = \App\challenge2021::all()->whereNotNull('remarks');
        }

//        dd($remarks);
        return view('exports.remarks', compact("remarks"));
    }
}
