<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class settings extends Model
{
    static public function getSeason(){
        return \App\settings::all()
            ->where('key', 'season')
            ->first()->value;
    }

    static public function getFinalName(){
        return \App\settings::all()
            ->where('key', 'finalName')
            ->first()->value;
    }

    static public function getRegister(){
        return \App\settings::all()
            ->where('key', 'register')
            ->first()->value;
    }

    static public function getGamePublic(){
        return \App\settings::all()
            ->where('key', 'gamePublic')
            ->first()->value;
    }

}
